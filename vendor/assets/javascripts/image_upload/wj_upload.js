/***
WJ_UPLOAD.JS
imparts image upload functionality to any dom element.

***/
(function(){
    
    var $, Wimage;
    $ = window.jQuery;


    /***
    LISTENERS
    ***/

    //DRAG_AND_DROP Listener
    $(document)
    .on('dragover', function (e) {
      e.preventDefault()
      e = e.originalEvent
      e.dataTransfer.dropEffect = 'copy'
    })
    .on('drop', commonHandler)


    
    //PASTE LISTENER
    $('._paste_image').on('pasteImage', function(ev, data){
        commonHandler(data.blob);
    }).on('pasteImageError', function(ev, data){
        alert('Oops: ' + data.message);
        if(data.url){
          alert('But we got its url anyway:' + data.url)
        }
    });


    //FILE UPLOAD LISTENER.


    /***
    EVENT HANDLERS
    ***/
    commonHandler = function(blob){
        //it has to get the target.
        //it has to get the 
    }


    /***
    function added to jquery -> this is the function to call on whatever div you want to make image_uploadable
    @storage_service -> 'azure|aws'
    ***/
    $.fn.wimage_upload = function(storage_service){
        var el, j, len, ref;
        //this is the element or set of elements on which this function
        //is called using jquery.
        ref = this;
        for (j = 0, len = ref.length; j < len; j++) {
          el = ref[j];
          //create a new wimage.
          //then call the container css setup on it.
          var wimage = new Wimage(el,storage_service);
          wimage.draw_layout();
        }
    }

    /***
    this is a class definition.
    ***/
    Wimage = (function(){
        Wimage.prototype._container = null;
        Wimage.prototype._drag_and_drop_image = null;
        Wimage.prototype._paste_image = null;
        Wimage.prototype._upload_from_pc_image = null;
        Wimage.prototype._image_result = null;
        Wimage.prototype._id = null;
        Wimage.drag_and_drop_image_template = _.template("<div id='drag_and_drop_image_<%= id %>' class='_drag_and_drop' style='width:30%;min-height:40px; padding:30px;'>Drag and Drop files here</div>");
        Wimage.paste_image_template = _.template("<div id='paste_image_<%= unique_id %>' class='_paste_image'style='width:30%;min-height:40px; padding:30px;'>Right click to paste image</div>");
        Wimage.upload_from_pc_image_template = _.template("<div id='upload_from_pc_image_<%= unique_id %>' class='_upload_from_pc_image' style='width:30%;min-height:40px; padding:30px;'>Upload an image from your computer</div>");
        Wimage.image_result_template = _.template("<div id='result_<%= id %>' style='display:none;'></div>")
        Wimage.unique_id = function(){
            return '_' + Math.random().toString(36).substr(2, 9);
        };
        Wimage.extract_id = function(id_string){
        
        }
        function Wimage(container,storage_service){
            this._container = container;
            this._id = Wimage.unique_id;
            this._drag_and_drop_image = Wimage.drag_and_drop_image_template({unique_id: this._id});
            this._paste_image = Wimage.paste_image_template({unique_id: this._id});
            this._upload_from_pc_image = Wimage.upload_from_pc_image_template({unique_id: this._id});
            this._image_result = Wimage.image_result_template({unique_id : this._id});
            //this._container.append(this._drag_and_drop_image).append(this._paste_image).append(this._upload_from_pc_image).append(this._image_result);
        }

        Wimage.prototype.draw_layout = function(){
            //'this' accesses the instance.
            
        }

        return Wimage;
    });

}).call(this);
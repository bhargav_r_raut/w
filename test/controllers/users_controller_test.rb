require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  test "should get before_confirm" do
    get :before_confirm
    assert_response :success
  end

  test "should get after_oauth_sign_up" do
    get :after_oauth_sign_up
    assert_response :success
  end

end

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)



t = Test.new
t.name = "yashada"
i = Identity.new
i.uid = "1234"
i.provider = "facebook"
t.identities << i
t.save!

t = Test.new
t.name = "girija"
i = Identity.new
i.uid = "3455"
i.provider = "google oauth2"
t.identities << i
t.save!

t = Test.new
t.name = "radhika"
i = Identity.new
i.uid = "3975"
i.provider = "google oauth2"
t.identities << i
t.save!
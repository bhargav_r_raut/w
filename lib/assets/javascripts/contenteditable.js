var focused_el_id;
$(document).on({
    mouseenter: function () {
    	$(this).css('cursor','text');
        return true;
    },
    mouseleave: function(){
    	$(this).css('cursor','default');
        return true;
    },
    focus: function(event){
        focused_el_id = $(this).attr("id");

        if(sign_in_required(event)){
            $(this).blur();
            return;
        }
        
        var element_id = $(this).attr("data-element-id");
    
        $(this).removeClass("virgin");
        $(this).addClass("edited");
        

        //show warnings for the previous unit. if applicable.
        //var prev_el_hash = get_previous_unit(element_id);        
        //console.log("prev el hash is");
        //console.log(prev_el_hash);
        //if(!($.isEmptyObject(prev_el_hash))){
        //   var prev_ceditable = prev_el_hash["p_ceditable"];
        //    var prev_wholder = prev_el_hash["p_wholder"];
        //    var prev_el = prev_el_hash["p_element"];
        //    validate_unit(prev_el.attr("data-element-id"),event);           
        //}  
      
        return true;
    },
    focusout: function(event){
        var element_id = $(this).attr("data-element-id");
        if($(this).text().trim() == ""){
            $(this).removeClass("edited");
            $(this).addClass("virgin");
                
            
            //NOT APPLICABLE TO THE INTRO BOX, BECAUSE IT DOES NOT HAVE ANY EXAMPLE ICON.
            //THAT IS TRIGGERED FROM THE CORNER TITLE ITSELF.
            if(!($(this).hasClass("intro"))){
                $("#" + element_id + "_example_icon").show();
            }

           
            $(this).attr("placeholder",$(this).attr("data-placeholder"));
        }
        else{
            
             $("#" + element_id + "_title_warning").hide();

            //NOT APPLICABLE TO INTRO BECAUSE IT DOES NOT HAVE EXAMPLE ICON.
            //THAT IS TRIGGERED FROM THE TITLE OF THE CORNER.
            
            if(!($(this).hasClass("intro"))){
                $("#" + element_id + "_example_icon").hide();
            }
            
            $(this).attr("placeholder","");  
        }

        //do dirty / clean.
        if($(this).text().trim() !== $(this).attr("data-temp-value")){
            $(this).addClass("dirty");
                
            var sibling_weather_holder_clicked_val = sibling_weather_holder_clicked(element_id);
            
            console.log("sibling weather holder clicked val is:");
            console.log(sibling_weather_holder_clicked_val);

            if(!sibling_weather_holder_clicked(element_id)){
                console.log("calling submit from contenteditable focusout");
                submit_corner(event);
            }
            else{
                console.log("skipping submit from ceditable");
            }

        }
        else{
            $(this).removeClass("dirty");
        }

        return true;

    },
    keyup: function(e){

        switch (e.keyCode) {
            case 8:
               
                $(this).children(".br_tag").show();
                
            break;

            default:
            break;
        }

        return true;
    }
},'[contenteditable]');
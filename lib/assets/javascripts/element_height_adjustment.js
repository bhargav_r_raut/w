/****
Used for contenteditable card elements, which also have a card reveal.
when the activator is clicked, we need to get the effective height of whatever is the content that the card-reveal will show and then change the height of the card to match that

it also has to handle storing the initial height of the card, and the calculated height of the card-reveal in data-attributes on the activator itself.

this is because using offsets we get the effective height of the card-reveal only the first time, and later by the offsets methods it just becomes zero.

the technique used is to add a dummy div after the last element element in the card-reveal and get its offset and also the offset of the element at the top of the card-reveal element, and then use that difference to calculate and store the height.
****/

/****
@param[jquery Element] : card_reveal_element
@param[jquery Element] : marker_element

given the card_reveal jquery element and the marker element at its bottom, will return the height of the card_reveal.

@return[Integer]: height in pixels of the spam table element.
****/
function calculate_card_reveal_height(card_reveal_el,marker_element){
	return (marker_element.offset().top - card_reveal_el.offset().top);
}


/****
@param[jquery Element] : card_reveal_element
@param[jquery Element] : marker_element
@param[jquery Element] : activator_element.

calls the get_card_reveal_height function and sets the returned height in teh data-spam-table-height attribute of the activator_el.

@return: null
****/
function store_card_reveal_height(card_reveal_el,marker_element,activator_el){
	var card_reveal_element_height = calculate_card_reveal_height(card_reveal_el,marker_element);
	activator_el.attr('data-spam-table-height',card_reveal_element_height);
}

/****
@param[jquery Element] : contenteditable_element(either the corner or unit content box, which has the .card class on it.)
@param[jquery Element] : activator element.

sets the height of the contenteditable element on the data-ceditable-el-height attribute of the activator element.

@return: null.
****/
function store_ceditable_initial_height(ceditable_el,activator_el){
	var initial_height = ceditable_el.outerHeight();
	activator_el.attr('data-ceditable-el-height',initial_height);
}


/***
@param[String] : el_id ; the element id of the associated corner or unit.
@return[jquery Element] : returns the spam table jquery element.
***/
function get_card_reveal_element(el_id){
	var card_reveal_el = get_el_by_class_and_data_el_id('card-reveal',el_id);
	return card_reveal_el;
}

/***
@param[String] : el_id ;  the element id of the associated corner or unit.
@return[jquery Element]: returns the ceditable corner or unit element, that has the .card class.
****/
function get_ceditable_element(el_id){
	return get_el_by_class_and_data_el_id('card',el_id);
}

/***
@param[String] : el_id ;  the element id of the associated corner or unit.
@return[jquery Element]: returns the activator element, that has the .activator class, this is the element by clicking on which the card-reveal opens.
****/
function get_activator_element(el_id){
	return get_el_by_class_and_data_el_id("activator",el_id);
}


/***
@param[String] : el_id ;  the element id of the associated corner or unit.
@return[Integer]: returns the height of the ceditable element
***/
function calculate_ceditable_height(el_id){
	return get_ceditable_element(el_id).outerHeight();
}


/***
@param[String] : el_id ;  the element id of the associated corner or unit.
@return[Object]: returns the height of the ceditable element and the card_reveal_element.
***/
function get_ceditable_el_height_and_card_reveal_height(el_id){
	activator_el = get_activator_element(el_id);
	return {"ceditable_height" : activator_el.attr("data-ceditable-el-height"), "card_reveal_height" : activator_el.attr("data-spam-table-height")};
}


/***
@param[String] : el_id ;  the element id of the associated corner or unit.
@return: nil
***/
function set_ceditable_height(el_id,height){
	ceditable_el = get_ceditable_element(el_id);
	ceditable_el.css('height',height + 'px');
}


function get_card_reveal_element(el_id){
	return get_el_by_class_and_data_el_id('card-reveal',el_id);
}

function card_reveal_is_open(el_id){
	return get_card_reveal_element(el_id).is(':visible');
}

function card_reveal_is_closed(el_id){
	return get_card_reveal_element(el_id).is(':hidden');
}

function get_marker_element(el_id){
	return  get_el_by_class_and_data_el_id('card-reveal-bottom-marker',el_id);

}

/***
handle the toggle of the activator element.

- get card div height from activator.
- get table height from activator.
	- if they are not available
	- call set_card_reveal_height
	- call set_ceditable_initial_height
	- set ceditable height to required table height.
***/
$(document).on("click",".activator",function(event){
	
	var el_id = $(this).attr('data-element-id');
	
	var heights = get_ceditable_el_height_and_card_reveal_height(el_id);

	if(heights["ceditable_height"] == null){
		store_ceditable_initial_height(get_ceditable_element(el_id),get_activator_element(el_id));
		store_card_reveal_height(get_card_reveal_element(el_id),get_marker_element(el_id),get_activator_element(el_id));
	}
	set_ceditable_height(el_id,get_ceditable_el_height_and_card_reveal_height(el_id)["card_reveal_height"]);
		
	
});

/***
- get card div height from activator
- reset card div height to whatever was got.
***/
$(document).on('click',".card-reveal .deactivator",function(event){
	var el_id = $(this).attr("data-element-id");
	var heights = get_ceditable_el_height_and_card_reveal_height(el_id);
	set_ceditable_height(el_id,heights["ceditable_height"]);
	
});

var clicky;

$(document).mousedown(function(e) {
    // The latest element clicked
    clicky = $(e.target);
});

// when 'clicky == null' on blur, we know it was not caused by a click
// but maybe by pressing the tab key
$(document).mouseup(function(e) {
    clicky = null;
});
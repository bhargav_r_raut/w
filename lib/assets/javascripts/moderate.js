$(document).on('click','.approve',function(event){
	$(this).addClass("amber-text");
	$(this).next('.disapprove').first().hide();
	$(this).tooltipster('content','Approved');
});

$(document).on('click','.disapprove',function(event){
	$(this).removeClass("red-text");
	$(this).addClass("black-text");
	$(this).tooltipster('content','Marked As Spam');
	$(this).prev('.approve').first().hide();

});
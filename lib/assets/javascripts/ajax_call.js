function ajax_call(url,data_hash,method){
	var promise = $.ajax({
        beforeSend: function(){
          
        },
        url: url,
        headers: {
        Accept : "text/javascript; charset=utf-8",
          "Content-Type": 'application/json; charset=UTF-8'
        },
        type: method,
        data: JSON.stringify(data_hash),
        timeout: 50000,
        success: function(data) {
          
        },
        error: function() {
          
        },
        complete: function(){

        }
    });
    return promise;
}
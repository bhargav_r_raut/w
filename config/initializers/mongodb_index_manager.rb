Rails.configuration.after_initialize do
  	
	
	if User.exists?

		existing_index_names = User.collection.indexes.map{|c| c["name"]}

		##this index is used to see if the email already exists during sign up.
		if !existing_index_names.include? "email_index"
			User.collection.indexes.create_one({"email" => 1},:name => "email_index")
			Rails.logger.warn "Collection:#{User}: index:email_index did not exist and was created."
		end

		##this index is used for the first query during oauth, to check whether the user already has registered using oauth with us.
		if !existing_index_names.include? "identities_index"
			User.collection.indexes.create_one({"identities.uid" => 1, "identities.provider" => 1},:name => "identities_index")
			Rails.logger.warn "Collection:#{User}: index:identities_uid_index did not exist and was created"
		end

		##this is used in oauth while updating a user who is already signed in but is now signing in using oauth, we check while updating his identity that he does not have this identity, in case someone else tries to update in the meantime.
		if !existing_index_names.include? "id_and_identities_index"
			User.collection.indexes.create_one({"_id" => 1,"identities.uid" => 1, "identities.provider" => 1},:name => "id_and_identities_index")
			Rails.logger.warn "Collection:#{User}: index: id_and_identities_uid_index did not exist and was created"
		end

		##last step in oauth checks whether there is a user who has a verified email id the same as this one, and he does not have this identity, then we add this identity to him, and addtoset the other fields, if no such user exists, then we upsert the document, and the set on insert defines what is to be done.
		if !existing_index_names.include? "email_and_identities_index"

			User.collection.indexes.create_one({
				"email" => 1, "identities.uid" => 1, "identities.provider" => 1}, :name => "email_and_identities_index")
			Rails.logger.warn "Collection : #{User} index: email_and_identities_index did not exist and had to be created."


		end

	end

##########################################################################################
#########################################################################################
#INDEXES FOR CORNERS
#######################################################################################
#######################################################################################


if Corner.exists?

	
	existing_index_names = Corner.collection.indexes.map{|c| c["name"]}
	if !existing_index_names.include? "slugs_index"

			Corner.collection.indexes.create_one({
				"_slugs" => 1}, :name => "slugs_index")
			Rails.logger.warn "Collection : #{Corner} index: slugs_index did not exist and had to be created."

	end
else
	
end


end


=begin
#one index on email, uid, provider.
#one index on id,uid,provider
#one index on uid, provider
=end

Rails.application.routes.draw do
  get 'images/home' , :action => "home", :controller => "images"
  
  ##THESE TWO ROUTES WILL BE NEEDED FOR FINE_UPLOADER_AZURE FUNCTIONALITY ANYWHERE
  get 'images/azure_sas_uri', :action => "azure_sas_uri", :controller => "images"
  post 'images/azure_success', :action => "azure_success", :controller => "images"
  ##END.

  get 'login/new'

  get 'elements/build'

  get 'element/build'

  resources :units
  
  resources :lists, except: [:index,:new,:create,:show,:edit,:update] do 
    collection do 
      get 'element_spam_list'
    end
  end


  get 'categories/search'

  resources :tests, only: [:index,:new,:create,:show,:edit,:update] do
    collection do 
      get 'change'
    end
  end

  root 'home#index'

  resources :categories do 
    collection do 
      get 'search/:q', :action => "search", :as => 'search'
    end
  end

  

  resources :corners do 
    
  end

  resources :elements do 
    collection do 
      post 'edit_image'
    end
  end

  resources :reports, only: [:new]

  resources :home, only: [:index]

  

  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" , :registrations => "users/registrations", :confirmations => "users/confirmations", :passwords => "users/passwords", :sessions => "users/sessions"}

  resources :users, except: [:show, :edit, :update, :destroy, :index, :new, :create] do

    collection do
        get 'sign_in_failed','email_exists','nickname_exists','password_strength','oauth_sign_in_failed','email_matches_current'
    end

    get 'profile', on: :member
  
  end



  

  resources :topics do 
    collection do
      get 'search/:q', :action => "search", :as => 'search'
    end
  end




  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     #   end
end

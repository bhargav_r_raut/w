class CornersController < ElementsController

  respond_to :js,:html

  def index

    @corner = Corner.order_by(:created_at => 'desc').first
    redirect_to corner_path(@corner)

  end


  def new
   
      @r = Response.new
      @r.corner = Corner.new
      @r.units = [Unit.new]
      @r.errors = {}
      @r.messages = {}
   
  
  end


  def edit

    @corner = Corner.find(params[:id])

  end

  def show
    @r = Response.new
    e = Element.any_of({:corner_id => BSON::ObjectId(params[:id])},{:id => BSON::ObjectId(params[:id])}).order_by(:score => "desc")

    @r.corner = e[0]
    
    ##this should be sorted by score.
    @r.units = e[1..-1]
    #puts "r corner is:"
    #puts JSON.pretty_generate(@r.corner.attributes)
    #puts "units are"
    @r.units.each do |u|
    #puts JSON.pretty_generate(u.attributes)
    end
    #gets.chomp
    if @r.corner
      puts "the corner contributor ids are:"
      puts @r.corner.contributor_ids.to_s
      @r.creator = User.find(@r.corner.contributor_ids.last[0])
    end

    render :template => "corners/new"
  end


  ##this was called on create if there was not registered user.
  def add_to_db
    current_user ?  process_element(session[:pending] ? session[:pending] : create_permit_params) : add_params_to_session(create_permit_params)
  end


 
end

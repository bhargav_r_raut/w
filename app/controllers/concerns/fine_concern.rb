module FineConcern

  extend ActiveSupport::Concern

  included do
    respond_to :html,:js,:json
    #prepend_before_action :set_devise_mapping_for_omniauth, only: [:omni_common]
    #prepend_before_action :do_before_request, only: [:omni_common]
    #attr_accessor :resource
    #helper_method :omniauth_failed_path_for
  end

  def azure_sas_uri
  	prms = permitted_params
  	if check_method_and_url
  	  sas = Azure::Storage::Core::Auth::SharedAccessSignature.new(ENV["AZURE_STORAGE_ACCOUNT"],ENV["AZURE_STORAGE_ACCESS_KEY"])
  		options = {}
  		options[:service] = 'bqft'
  		options[:resource] = 'sco'
  		options[:permissions] = 'racwdl'
  		o = sas.generate_account_sas_token(options)
  		sas_uri = prms[:bloburi] + "?" + o.to_s
      respond_to do |format|
        format.html
        format.json { render :text => sas_uri }
      end
    end
	end

  def azure_success
      respond_to do |format|
        format.html
        format.json { render :text => "success" }
      end
  end
  

  ##placeholder method can be overriden to decide if the method and url is permissible
  def check_method_and_url
  	true
  end

  def permitted_params
    puts "params before permitted params are:"
    puts params.to_s
  	params.permit(:bloburi, :_method, :qqtimestamp)
  end

end
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  rescue_from ActionDispatch::Cookies::CookieOverflow, :with => :handle_cookie_overflow_exception

  #before_filter :configure_permitted_parameters, if: :devise_controller?


  after_filter :store_location

  append_after_filter :set_csrf_headers

  def set_csrf_headers
    if request.xhr?
      # Add the newly created csrf token to the page headers
      # These values are sent on 1 request only
      response.headers['X-CSRF-Token'] = "#{form_authenticity_token}"
      response.headers['X-CSRF-Param'] = "#{request_forgery_protection_token}"
    end
  end

  def store_location
    # store last url - this is needed for post-login redirect to whatever the user last visited.
    if (request.fullpath != "/users/sign_in" &&
        request.fullpath != "/users/sign_up" &&
        request.fullpath != "/users/password" &&
        request.fullpath != "/users/sign_out" &&
        !request.xhr?) # don't store ajax calls
      session["user_return_to"] = request.fullpath 
    end
  end

  

  def after_sign_in_path_for(resource)
    Rails.logger.debug("came to after sign in path for.")
    sign_in_url = new_user_session_url
    Rails.logger.debug("this is the sign in url #{sign_in_url}")
    if request.referer == sign_in_url
      Rails.logger.debug("the sign in url is the same as the referrer.")
      super
    else
      #Rails.logger.debug("we go to the stored location")
      #Rails.logger.debug("stored location for is: #{stored_location_for(resource)}")
      #Rails.logger.debug("referrer is: #{request.referrer}")
      stored_location_for(resource) || request.referer || root_path
    end
  end


  protected

  def configure_permitted_parameters
  

    devise_parameter_sanitizer.permit(:sign_in) do |user|
      user.permit(:email,:password,:remember_me)
    end

    devise_parameter_sanitizer.permit(:sign_up) do |user|
      user.permit(:email,:password,:remember_me,:password_confirmation,:name)
    end

  end 

  
  def get_empty_identity

  	i = Identity.new(:uid => "", :provider => "", :email => "")
  	return i.attributes.except("_id")

  end

  def from_view(view,klass)

 	if !view.nil? && view.count > 0

 		user = Mongoid::Factory.from_db(klass,view.first)
 		return user

 	else

 		return nil

 	end

 end

 def from_bson(bson_doc,klass)

 	if !bson_doc.nil?

 		user = Mongoid::Factory.from_db(klass,bson_doc)
 		return user

 	else

 		return nil

 	end


 end




end

class UsersController < ApplicationController
  def before_confirm
    
  end

  def after_oauth_sign_up
  	
  end

  def oauth_sign_in_failed
    puts "oauth sign in failure"
    render :template => "users/sessions/_oauth_sign_in_failed"
  end

  def sign_in_failed
    #puts "routing sign in failure"
  	#render :template => "users/sessions/_sign_in_failed", :formats => [:js]
    @case = "sign_in_failed"
    @login_opts = {}
    render :template => "/modals/login", :locals => {:login_opts => @login_opts, :case_it => @case}, :formats => [:js] and return

  end

  ##method responds for checking if a given email id already exists, when a new user signs up.
  def email_exists
  	a = User.collection.count({"email" => params[:email]},{:limit => 1})
  	
  	respond_to do |format|
  		if a == 0
  			format.json {head :ok}
  		else
  			format.json {head :no_content}
  		end
  	end

  end


  def email_matches_current

    respond_to do |format|
      puts "current user email is: #{current_user.email}"
      puts "params email is: #{params[:email]}"
      if current_user.email == params[:email]
        format.json {head :ok}  
      else
        format.json {head :no_content}
      end
    
    end

  end

  def password_strength
  	h = {"password_strength" => PasswordStrength.test("johndoe", "mypass").status}
  	respond_to do |format|
  		format.json{render json: h.to_json}
	 end
  end

  def profile
    render :template => "users/profiles/home"
  end

  

end

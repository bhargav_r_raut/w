class ElementsController < ApplicationController
  
  def edit_image
    pa = permitted_params.with_indifferent_access
    if pa[:corner]
      el = Corner.new(pa[:corner]).image_id_matches?
    elsif pa[:units]
      units = pa[:units].map{|u| u = Unit.new(u)}
      el = units[0].image_id_matches?
    end
    render :partial => "corners/edit_image.js.erb", locals: {el: el}
  end

  ##overridden
  def new

  end

  def permitted_params   
      params.permit(:utf8, :remotipart_submitted, :authenticity_token, "X-Requested-With", "X-Http-Accept",:units => [:_id,:title,:content,:image,:link,:link_title,:icon,:image_file_name,:image_content_type,:image_file_size,:image_fingerprint,:image_updated_at,:image_url,:corner_id,:version,:image_id,:image_crop_x, :image_crop_y, :image_crop_w, :image_crop_h,:spam => [:image, :content, :title, :link]],:corner => [:_id,:title,:content,:image,:image_url,:link,:link_title,:image_file_name,:image_content_type,:image_file_size,:image_fingerprint,:image_updated_at,:version,:image_id,:image_crop_x, :image_crop_y, :image_crop_w, :image_crop_h,:spam => [:image, :content, :title, :link]])
  end


  def update
    commit
  end

  
  def create
    commit
  end


  def commit
   
    p = permitted_params.with_indifferent_access
  
    #puts "the params coming in : #{p.to_s}"
    Rails.logger.debug("the params coming in are: #{p.to_s}")


    errors = {}
  
    ##create the corner
    corner = p[:corner].nil? ? nil : Corner.build(p[:corner],current_user) 
    
    #puts "corner built is :"
    #puts corner.attributes.to_s

    errors = merge_errors(corner,errors) 

    ##create the units
    units,errors = get_units(p,corner,errors)
    units.each do |unit|
      merge_errors(unit,errors)
    end

    ##build the response object.
    @r = Response.new
    @r.corner = corner
    @r.units = units
    @r.errors = errors
    @r.messages = {}  

    puts "these are the errors."
    puts JSON.pretty_generate(@r.errors)
  
    respond_to do |format|
      format.html {render "new"}
      format.js {render :template => "/corners/_corner_created", :locals => @r.attributes, :status => 200}
    end

  end

  private 

 

  


  ##basically first check if all the non nil corner ids on the units are the same otherwise raise a generic error and return
  ##after that, if the unit does not have a corner id, i.e its nil, then check if we have a corner, and assign that corners id, otherwise let the units corner id remain nil, and that will be handled in the unit validation.
  ##if the unit has a corner id, and if we also have a corner, then the two should be the same, otherwise make the units corner id nil, and that will automatically be rejected in the validation.
  ##finally initiale a bson object id from the units corner id, since that is a string, if on the other hand we provided it from the corner then it is already a bson object id.
  def get_units(p,corner,errors)
   
    if !p[:units].nil?

      ##first check if all the non nil corner ids are the same
      if p[:units].map{|c| c[:corner_id]}.uniq.compact.size > 1
        errors["units"] = {"corner_ids" => "the corner ids on the units are not the same"}
        return [],errors
      end
      
      p[:units].map!{|c|
        ##if there is no corner id on the unit, and provided that we have a corner coming into the params, then we assign it.
        if(c[:corner_id].blank? && !corner.nil?)
          c[:corner_id] = corner.id
        elsif !c[:corner_id].blank?
         
          ##if the corner is not nil, then the units corner id should be the same as it, otherwise make it nil.
          if !corner.nil? && (c[:corner_id] != corner.id.to_s)
            c[:corner_id] = nil
          else
            begin
              c[:corner_id] = BSON::ObjectId.from_string(c[:corner_id])

            rescue => e
              c[:corner_id] = nil
            end
          end
        end
        c = Unit.build(c,current_user)
      }

      return p[:units],errors

    end

    return [],errors
    
  end 



  ##given an element and the errors hash.
  ##if the el is nil, then the errors are returned
  ##if the errors hash does not contain the el, then it is initiated .
  ##if the errors hash contains the el then the existing errors are merged with the new errors.
  def merge_errors(el,errors)
    if !el.nil?
      if !el.errors.messages.empty?
        if errors[el.id.to_s].nil? 
           errors[el.id.to_s] = el.errors.messages
        else
          errors[el.id.to_s].merge!(el.errors.messages)
        end
      end
    end
    return errors
  end

  ##checks the response from the get_units, and sees whether the situation is such that
  ##units have been provided and none of them have a corner, in which case true is returned.
  ##this is used to force the creation of a new corner.
  def units_need_corner?(pl_units)

    if pl_units[1] == nil
      if pl_units[0].empty?
        Rails.logger.debug("no corner id in units, because no units, shoudl return false")
      else
        Rails.logger.debug("no corner id in units, but units exist, should return true")
      end
    else
      Rails.logger.debug("the units had a corner id, should return false.")
      return false
    end


    pl_units[1] == nil ? (pl_units[0].empty? ? false : true) : false

  end

  ##adds the corner params to the session
  ##returns the js erb telling the user to sign in.
  def add_params_to_session(p)

      if p.to_s.bytesize > 1000
        r = Response.new(:messages => {:user => "you need to sign up to continue", :content => "the content you posted is too large"})

      else
        Rails.logger.debug("this will be added to the session")
        r = Response.new(:messages => {:user => "you need to sign up to continue", :content => "your content will be stored in a cookie, and automatically published after you sign up."})
        session[:pending] = p
      end

      render :template => "/modals/_sign_in_needed_modal", :locals => r.attributes, :formats => [:js]

  end

end

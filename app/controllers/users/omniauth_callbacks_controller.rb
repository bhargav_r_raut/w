class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

	before_action :set_oauth_in_flash

	def google_oauth2

   		omni_hash = request.env["omniauth.auth"]

   		Rails.logger.debug("entered oauth google, controller.")

   		omni_common(omni_hash["info"]["email"],omni_hash["uid"],omni_hash["provider"])
	   

   end


   def facebook
   		
		omni_hash = request.env["omniauth.auth"]


   		Rails.logger.debug("entered oauth facebook, controller.")

   		omni_common(omni_hash["info"]["email"],omni_hash["uid"],omni_hash["provider"])


   end


   def omni_common(email,uid,provider)


   		identity = Identity.new(:provider => provider, :uid => uid, :email => email)

	   	##this index is used for the first query during oauth, to check whether the user already has registered using oauth with us.
	   	existing_oauth_users = 
	   	User.collection.find(
	   		{"identities" =>
	   					 {"$elemMatch" => 
	   					 				{"provider" => provider, "uid" => uid
	   					 				}
	   					 }
	   		})
	 
	   	if existing_oauth_users.count == 1

	   		user = from_view(existing_oauth_users,User)

	   		if user.persisted?
	   			if user.respond_to?("skip_confirmation!")
	   				user.skip_confirmation!
	   			end
	   			Rails.logger.debug("came to sign in an redirec")
	   			sign_in_and_redirect user
	   		else
	   			
	   			redirect_to oauth_sign_in_failed_users_path

	   		end

	   	
	   	elsif current_user

	   		Rails.logger.debug("it is a current user trying to sign up with oauth.")
	   		##throw him to profile, he's an asshole.
			redirect_to after_oauth_sign_up_path_for(current_user)	   		

	   	else 

	   		Rails.logger.debug("no such user exists, trying to create a new user by merging the fields.")
	   		
	  		
	   		new_user_view = User.collection.find_one_and_update(
	   			{
	   				"email" => email,
	   				"identities" => {
	   					"$elemMatch" => {
	   						"uid" => {
	   							"$ne" => uid
	   						},
	   						"provider" => {
	   							"$ne" => provider
	   						}	   						
	   					}
	   				}
	   			},
	   			{
	   				"$push" => {
	   					"identities" => identity.attributes.except("_id")
	   				},
	   				"$setOnInsert" => {
	   					"email" => email,
	   					"password" =>  Devise.friendly_token(20)
	   				}
	   			},
	   			:return_document => :after,
	   			:upsert => true
	   		)


  			new_user = from_bson(new_user_view,User)
  			

  			##sign in and send to the user profiles path.

  			if new_user.persisted?
  				if new_user.respond_to?("skip_confirmation!")
  					new_user.skip_confirmation!
  				end
  				sign_in new_user
  				redirect_to after_oauth_sign_up_path_for(new_user)
  			else
  				redirect_to oauth_sign_in_failed_users_path
  			end

	   	end


   end


   private

   def set_oauth_in_flash
   	flash[:request_type] = "oauth"
   end

   def after_oauth_sign_up_path_for(resource)
   		profile_user_path(resource.id)
   end

end



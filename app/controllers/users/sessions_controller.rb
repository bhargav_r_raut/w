class Users::SessionsController < Devise::SessionsController

	def new
		puts "called new"
		super
	end
	
	  def create
	  	puts "-----------------------------CAME TO CREATE ------------------------"
	    self.resource = warden.authenticate!(auth_options)
	    puts "Came past authenticate"
	    set_flash_message!(:notice, :signed_in)
	    sign_in(resource_name, resource)
	    yield resource if block_given?
	    respond_with resource, location: after_sign_in_path_for(resource)
	  end

end
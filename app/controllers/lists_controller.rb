class ListsController < ApplicationController
	def element_spam_list
		## remember to initialize a new list object.
		## and then find the users list.
		## and then do this.
		## remember to render the users/profiles/users_list.js.erb
		@l = List.new(permitted_params)
		@l.get_el_attribute_spam_users
		render :partial => "users/profiles/users_list.js.erb", locals: {users_list: @l.users_list}		
	end

	##from and to are to decide from which 
	def permitted_params
		if action_name == "element_spam_list"
			params.permit(:element_id, :spam_for_attribute, :upto_time, :number)
		end
	end
end

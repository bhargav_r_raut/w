class UnitsController < ElementsController
  
  respond_to :html, :js  

  def new
    @unit = Unit.new

    

    if params[:corner_id].blank? || !Element.id_is_valid?({:_id => params[:corner_id]})
      @unit.errors.add("corner_id","corner id was invalid")
    else
      @unit.corner_id = params[:corner_id]
    end

    respond_with @unit
  end

end

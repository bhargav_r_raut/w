module ApplicationHelper
	def epoch_to_readable(epoch)

		current_time = Time.now.to_i
		diff = current_time - epoch
		##so if its less than an hour, then tell in minutes.
		case 

		when diff < 300
			return "- just now"
		when diff < 3600
			return "- #{diff.to_i/60} minutes ago"
		when diff < 86400
			return "- #{diff.to_i/3600} hours ago"
		else
			dt = Time.at(epoch)
			human_date = dt.strftime("%A, %b %d")
			return "- on #{human_date}"
		end

	end

	def weather_icon_from_integer(icon_number)
		
	end
	
	
	##this calls the element to see if there is anything pending approval and provided that the current user is present and can approve whatever needs to be approved it returns true.
	##it also returns the hash of whether to show the image, link or content, and title,
	##based on whether they are clean, or if the show approv is true.

	##has entity -> no spam. 
	##has entity -> verified
	##has entity ->

	def modifications_for_testing_purposes(response)
		response[:show_spam_button] = true
		return response
	end

	def get_show_permissions(element,intro)
		#Rails.logger.debug("getting permissions for element:")
		#Rails.logger.debug(JSON.pretty_generate(element.attributes))

		approver_show = show_approve?(element)
		
		response = {:show_approve => approver_show, :show_spam_button => false}

		["image","link","content","title"].each do |type|
			response[type] = {:show_entity => show_entity?(element,approver_show,type),
				:enable_spam => enable_entity_spam?(element,type,show_entity?(element,approver_show,type)), :change_or_add_entity => change_or_add_entity(element,type,intro)}
			if response[type][:enable_spam]
				response[:show_spam_button] = true
			end
		end

		response[:show_options_bar] = show_options_bar?(response,element)

		response = modifications_for_testing_purposes(response)

		return response


	end

	def show_options_bar?(response,element)

		return (response[:show_spam_button] || response["link"][:change_or_add_entity] || (element.is_a_unit? && response["image"][:change_or_add_entity]))

	end

	##retunrs either falso  or a string which can be used as the tooltip in case the user can change or add the entity.
	def change_or_add_entity(element,type,intro)
		
		##this should cover the case for a new entity.
		if !element.has_entity?(type) && (current_user ? current_user.can_add? : true)
			
			return element.add_new_or_upload_entity(type,intro)
		
		elsif element.has_clean_entity?(type) && (current_user ? current_user.can_change?(element,type) : false) 

			return element.change_entity(type,intro)

		elsif current_user ? current_user.can_change?(element,type) : false
			##the current user is the contributor then it should be possible for him to change the image, so can_change? will have to be called, since it checks lock status.
			return element.change_entity(type,intro)
		else
			return false
		end
	

	end

	

	def show_approve?(element)
		
		if current_user
			
			return current_user.can_approve?(element)
		end
		return false
		
	end


	def enable_entity_spam?(element,type,show_entity)
		return show_entity && (current_user ? (current_user.is_not_contributor?(element,type) && current_user.can_disapprove?) : true)
	end

	##provided that there is an image, and 
	##it is pending moderation + approver is present.
	##it is a clean image
	##or if the current user is the contributor of the entity and the entity exists.
	##or if the entity is blank and the current user can change or add 
	##spam will only be enabled for a non signed in user if the enitity is clean.

	##this last condition applies only to those elements which have placeholders.
	##actually only to the title.
	##no 'type' i.e either (title, content, image or link), but the current user is the element creator, provided that the 

	def show_entity?(element,approver_show,type)

		#Rails.logger.debug("element type is:" + element._type)
		#Rails.logger.debug(element.title)
		#Rails.logger.debug("type is:" + type)
		#Rails.logger.info("is the enitty pending moderation and do we show the approver?")
		#Rails.logger.info(((element.entity_pending_moderation?(type) && approver_show)).to_s)
		#Rails.logger.info("is this a clean entity")
		#Rails.logger.info(((element.has_clean_entity?(type))).to_s)
		#Rails.logger.info("this element has entity and if we have a current user then is he a contributor")
		#Rails.logger.info(((element.has_entity?(type) && (current_user ? current_user.is_contributor?(element,type) : false))).to_s)
		#Rails.logger.debug("does it have an entity")
		#Rails.logger.info(element.has_entity?(type))
		#Rails.logger.debug("is there a current user and is he a contributor")
		#Rails.logger.debug(current_user ? current_user.is_contributor?(element,type) : false)

		return ((element.entity_pending_moderation?(type) && approver_show) || (element.has_clean_entity?(type)) ||  (element.has_entity?(type) && (current_user ? current_user.is_contributor?(element,type) : false)) || (!element.has_entity?(type) && !element.new_record? && (type == "title") && current_user.is_element_owner?(element)))
	end

	def sun_status(element)
    ##should return blank if yes, and display_none if no
    return (element.has_an_icon? && element.has_not_sun_icon?)? "display_none" : (element.has_sun_icon? ? "set" : "dull")
    end

    def rain_seperator_status(element)

    	return (rain_status(element) == "set" || rain_status(element) == "display_none")? "display_none" : ""

    end

  	def rain_status(element)
    	return (element.has_an_icon? && element.has_not_rain_icon?)? "display_none" : (element.has_rain_icon? ? "set" : "dull")
  	end

  	def storm_seperator_status(element)

  		return (storm_status(element) == "set" || storm_status(element) == "display_none")? "display_none" : ""

  	end

  	def storm_status(element)
    	return (element.has_an_icon? && element.has_not_storm_icon?)? "display_none" : (element.has_storm_icon? ? "set" : "dull")
  	end

	

end

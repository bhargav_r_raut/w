module CornersHelper

	
	## @used_in: corners/_card_reveal.html.erb
	## 		   : spam_changed.js.erb
	## @param[Element] el : the current element
	## @param[User] user : the current user
	## @param[Integer] count : the count of the spammed attribute
	## @param[String] attribute_marked_for_spam : the attribute marked for spam namely -> title/content/link/image
	## @returns[String] : a string saying how many people have marked this attribute as spam.
	def n_users_marked_this_as_spam(el,curr_usr,count,attribute_marked_for_spam)
		#puts "count is: #{count}"
		#puts "type is: #{attribute_marked_for_spam}"
		#puts "title contributor ids"
		#puts el.title_spam_contributor_ids

		#puts "content contributor ids"
		#puts el.content_spam_contributor_ids

		if el.attribute_marked_for_spam_contains_curr_usr(attribute_marked_for_spam,curr_usr)
			if count == 1
				"You marked this as spam" 
			elsif count == 2
				"You and 1 other person marked this as spam"
			elsif count > 2
				"You and " + ("other".pluralize(count - 1)) + " marked this as spam"
			else
				""
			end
		else
			
			if count > 0
				"user".pluralize(count) + " marked this as spam"
			else
				""
			end
		end
	end


	## @used_in:corners/_spam_changed.js.erb
	## @param[Element] : the current element.
	## @param[User] : the curr_usr
	## @return[String] : this is basically a hash that has been jsonified, 
	## it has the following structure:
	## two basic keys
	## =>  total - always contains only one key
	## i.e the id of the span element, that is seen in the options bar, under the spam_icon, which shows the total spam_count for this element.
	## the value of this key is the total_spam_count for this element.
	## =>  individual - always contains only one key.
	## i.e the id of the spam_icon in the card reveal spam_table.
	## the value of this key is either true/false, depending on whether the current user has marked in the previous commit an attribute for spam.
	def spam_details(el,curr_usr)
		attribute_marked_for_spam = el.get_spammed_attribute
		total_spam_count = el.get_total_spam_count
		response = {}
		response["total"] = {}
		response["total"][el.id.to_s + "_spam_count_"] = total_spam_count
		response["individual"] = {}
		response["individual"][el.id.to_s + "_spam_icon_" + attribute_marked_for_spam] = el.attribute_marked_for_spam_contains_curr_usr(attribute_marked_for_spam,curr_usr)
		JSON.generate(response).html_safe
	end

	

	

	## @used_in:corners/_corner_created.js.erb
	## In case spam is incremented, we do not re-render the corner and units.
	## We only increment the requisite attribute for which spam was marked.
	## Checks each spam attribute to see if it was changed.
	## logic used is to flatten corner and units into a single array, remove any nil elements, and keep those elements which have a spam_contributor_attribute present in the previous_changes of the element.
	## previous_changes is a rails method that performs dirty tracking.
	## for this we call a method in the Element.rb file called spam_attributes_added.
	## @param[Corner] c : Corner element
	## @param[Array] units: Array of Unit elements
	## @return[Element] : returns first element out of array of elements, which have had their spam attributes changed.
	def spam_attributes_changed?(c,units)
		[c,units].flatten.compact.keep_if{|c|
			c.spam_attributes_added.size > 0
		}[0]
	end

	## @used_in: corners/_card_reveal.html.erb
	## Decides whether a table row should be shown or not.
	## While deciding to show the table rows , we only want to show rows for those attributes which exist. 
	## The problem with images is that, if there is no image, then the placeholder image url becomes /missing.png. 
	## So it passes the check for the val[0] to exist, falsely.
	## So for images we further add a check to see if has_image? returns true.
	## In case of other attributes, as long as the val[0] exists, we return true.
	## @param[Array] val : Consists of two elements, at zero position is the  attribute value and and position one is the attribute name . eg: [title_value,"title"]
	## @param[Element] : the corner or unit.
	## @return[Boolean] : if the attribute value is nil, then false. If the attribute value is not nil, then if it is an image and an image exists -> then true, otherwise true.
	def show_spam_row(val,c)
		if val[0]
			if val[1] == "image"
				c.has_image?
			else
				true
			end
		else
			false
		end
	end

	## @used_in: corners/_spam_icon.html.erb
	## @param[Element] c : instance of Element , that is to be rendered in the view
	## @param[String] type : either "image","title","content","link"
	## @return[String] : the id of the attribute, or empty string "" if no id exists.
	def get_attribute_id(c,type)
		type_id = type + "_id"
		if tid = c.send(type_id)
			tid
		else
			""
		end
	end

end

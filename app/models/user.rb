class User

  LOCK_REPUTATION = 5000
  CHANGE_REPUTATION = 500
  APPROVE_REPUTATION = 20
  DISAPPROVE_REPUTATION = 0
  ADD_REPUTATION = 0
  SKIP_APPROVE_REPUTATION = 50

  include Mongoid::Document
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  devise :omniauthable, :omniauth_providers => [:google_oauth2,:facebook]
  ##ALERT ALERT ALERT ALERT , PLEASE UNCOMMENT THIS LINE, ITS ONLY TO AVOID 
  ##HAVING TO CONFIRM.
  ##devise :confirmable, :lockable
  ##devise :confirmable
  devise :lockable

  ##CUSTOM FIELDS.
  field :name,               type: String, default: ""
  field :what_do_you_do,      type: String, default: ""
  field :identities,          type: Array, default: []
  
  ## Database authenticatable
  field :email,              type: String, default: ""
  field :encrypted_password, type: String, default: ""

  ## Recoverable
  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time

  ## Rememberable
  field :remember_created_at, type: Time

  ## Trackable
  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String

  ## Confirmable
  field :confirmation_token,   type: String
  field :confirmed_at,         type: Time
  field :confirmation_sent_at, type: Time
  field :unconfirmed_email,    type: String # Only if using reconfirmable

  ## Lockable
  field :failed_attempts, type: Integer, default: 0 # Only if lock strategy is :failed_attempts
  field :unlock_token,    type: String # Only if unlock strategy is :email or :both
  field :locked_at,       type: Time
  field :reputation,      type: Float, default: 0

  ##this can change later on , for now just saying skip pending status if the reputation is greater than 50
  def set_element_status(element,type)
      #return reputation > SKIP_APPROVE_REPUTATION ? 1 : 4
      1
  end




  ##i can lock an element if my reputation is 5000 or if i am the creator or if i am the creator of a the corner in which the 'unit' belongs, provided taht the element is not already locked, and provided that it is not pending approval.
  def can_lock?(element)
    if element.is_a_corner?
     return !element.is_locked? && !element.any_moderation? && (id == element.creator_id || reputation >= LOCK_REPUTATION)
    else
     return !element.is_locked? && !element.any_moderation? && (id == element.creator_id || reputation >= LOCK_REPUTATION || (element.is_a_unit? && id == element.corner_creator_id))
    end
  end

  ##i can unlock an element if the element is locked and my reputation is greater than whoever has locked it.
  ##if the element is unlocked anyways then just return that i can unlock it.
  def can_unlock?(element)

    return ((element.is_locked? && (reputation >= element.locker_reputation)) || !element.is_locked?)

  end

=begin
##############OWNERSHIP DEFS, ONLY USED IN CAN_CHANGE.
###HERE WE CAN SEE IF THIS USER HAS BEEN BLOCKED FROM THIS CORNER.
=end
  def is_owner?(element,type)

    #puts "--checking is owner with type #{type}"

    if type =~/image/
         
        return is_image_owner?(element)
    elsif type =~/link/
        
        return is_link_owner?(element)
    elsif type =~/content/
         
        return is_content_owner?(element)
    elsif type =~/title/
         
        return is_title_owner?(element)
        
    elsif type =~/icon/
        return is_icon_owner?(element)
    end

  end

  ##returns true if you are the owner of the unit, or the corner of which it is a part.
  def is_element_owner?(element)
    
    #puts "----is element owner"

    if element.is_a_corner?
       #puts "-----its a corner so : id == element.creator_id --> #{id == element.creator_id}"
       return id == element.creator_id  
    else
      #puts "element creator id is:"
      #puts element.creator_id
      #puts "element corner creator id is"
      #puts element.corner_creator_id
      #puts "its a unit so: id == element.creator_id ---> #{id == element.creator_id} |or| #{id == element.corner_creator_id}"
      #return (id == element.creator_id || (id == element.corner_creator_id))
      if( id == element.creator_id)
        return true
      else
        ##get its corner, and then find out if that corner's creator is same as the current user.
        corner = Corner.find(element.corner_id)
        return corner.creator_id == id
      end
    end
  
  end
  

  ##i own the image if i own the elemnt in which it is placed or if i contributed it.
  def is_image_owner?(element)
    # puts "is image owner?"
     return is_element_owner?(element) || is_contributor?(element,"image")
  end

  def is_link_owner?(element)
   # puts "is link owner ?"
    return is_element_owner?(element) || is_contributor?(element,"link")
  end

  def is_content_owner?(element)
   # puts "is content owner ? "
    return is_element_owner?(element) || is_contributor?(element,"content")
  end

  ##only applicable to units.
  def is_title_owner?(element)
    #puts "---is title owner ?"
    return is_element_owner?(element) || is_contributor?(element,"title")
  end

  def is_icon_owner?(element)
    return is_element_owner?(element)
  end
=begin
#################################
OWNERSHIP DEFS END.
=end

  ##if the element is locked, and i can unlock it, 
  def can_change?(element,type)

      #puts "doing can change?"
      
      if is_owner?(element,type)
        #puts "--is owner: #{is_owner?(element,type)} , and can unlock: #{can_unlock?(element)}"
        ##if i can unlock this element
        ##and (either i am the owner of the element and if i can unlock the element, read definition of unlock above).
        return can_unlock?(element)
      
      else
        #puts "-can unlock?"
        ##if i am not the owner, then i can change the image if my reputation is of a certain level , and i can unlock the element.
        #puts "can unlock: #{can_unlock?(element)} and reputation : #{reputation} and change reputation: #{CHANGE_REPUTATION}"
        return (can_unlock?(element) && (reputation >= CHANGE_REPUTATION))
        
      end


  end

  ##AS LONG AS THERE IS SOMETHING IN THE ELEMENT THAT HE HAS NOT CONTRIBUTED, that still needs to be modified, and , AND 
  ##HE IS THE OWNER OF THE ELEMENT OR OF A CERTAIN REPUTATION, THEN HE CAN TURN APPROVER
  ##WHEN HE APPROVES, IT WILL ONLY APPLY TO WHAT HE HAS NOT CONTRIBUTED
  def can_approve?(element)
    pending_user_ids = element.pending_moderation_user_ids

    if pending_user_ids.empty?
      #Rails.logger.debug("pending user ids were empty")
      return false
    else
      #Rails.logger.debug("pending user ids were" + pending_user_ids.to_s)
      #Rails.logger.debug("ids after deleting present id:" + pending_user_ids.values.delete_if{|c| c == id}.to_s)
    
      #Rails.logger.debug("reputation:" + reputation.to_s)
      #Rails.logger.debug("is element owner" + is_element_owner?(element).to_s)

      return pending_user_ids.values.delete_if{|c| c == id}.size > 0 && (is_element_owner?(element) || reputation >= APPROVE_REPUTATION)
    end

  end


  def can_disapprove?

    ##anyone can disapprove,as long as he is signed in.
    return reputation >= DISAPPROVE_REPUTATION

  end


  ##if an image or a link or content does not exist, it can be added by anyone who is signed in, provided that his reputation is greater than add reputation.
  def can_add?
    #puts "came to can add"
    return reputation >= ADD_REPUTATION

  end

  ##this method should be called only after checking that the entity actually exists on the said element.
  def is_contributor?(element,type)
    #puts "-----here is the title contributor id:
     #{element.title_contributor_id}"
    if type =~/image/
        return id == element.image_contributor_id
    elsif type =~/link/
        return id == element.link_contributor_id
    elsif type =~/content/
        return id == element.content_contributor_id
    elsif type =~/title/
        return id == element.title_contributor_id
    end

  end

  ##this method should be called after ensuring that the required entity exists
  def is_not_contributor?(element,type)

    if type =~/image/
        return id != element.image_contributor_id
    elsif type =~/link/
        return id != element.link_contributor_id
    elsif type =~/content/
        return id != element.content_contributor_id
    elsif type =~/title/
        return id != element.title_contributor_id
    end

  end

end

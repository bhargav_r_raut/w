class Test
  include Mongoid::Document
  field :description, type: String
  field :publish	, type: Integer, default: 0	

  validates :description, presence: true, if: :publish_now

  has_many :images
 
  accepts_nested_attributes_for :images, :allow_destroy => true

  validates_associated :images

 



  def publish_now
  	return publish == 0 ? true : false
  end

end

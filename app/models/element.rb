class Element

  
  include Mongoid::Document
  include MongoidVersionedAtomic::VAtomic
  include Mongoid::Timestamps
  include Mongoid::Paperclip

  IMAGE_SPAM_COUNT = 2
  LINK_SPAM_COUNT = 1
  CONTENT_SPAM_COUNT = 2
  TITLE_SPAM_COUNT = 2
  CHANGABLES = ["title","content","link","link_title"]
  CHANGABLES_FOR_CONTRIBUTOR_ID = CHANGABLES + ["image"]
  CHANGABLES_FOR_ATTRIBUTE_ID = CHANGABLES_FOR_CONTRIBUTOR_ID - ["link_title"]
  IMAGE_ATTRIBUTES = ["image_file_name","image_fingerprint","image_content_type","image_file_size","image_updated_at","image_crop_x","image_crop_y","image_crop_w","image_crop_h"]
  ##########################################
  ##### CALLBACKS 
  ##########################################
  before_validation :remove_whitespaces
  ##########################################
  ##### CALLBACKS 
  ##########################################
  

  ##STATUS CODES:private

  ##0 -> pending
  ##1 -> approved
  ##-1 -> disapproved
  ##4 -> no value a.k.a not present.(this is the default)
    
  ## had to include this empty :cropped style, because otherwise paperclip does not run any processors
  ## got the solution from: https://github.com/rsantamaria/papercrop/issues/14
  has_mongoid_attached_file :image, styles: { :cropped => '' }
  crop_attached_file :image

  validates :id, presence:{message: "Something went wrong, please refresh the page and try again"}
  validates :title, length: {minimum:2, message: "Tell us what your corner is about!"}, :allow_nil => true
  
  validates_length_of :content, within: 2..200, too_long: 'pick a shorter content', too_short: 'pick a longer content', :allow_nil => true

  validates :link, format: {with: RubyRegex::Url, message: "the link url you entered is invalid"}, :allow_nil => true
  validates :link_title, length:{minimum:5, message: "the link title is too short, minimum required is 10 characters"}, :allow_nil => true
  validates :image_url, format: {with: RubyRegex::Url, message:"that url is invalid"}, :allow_nil => true
  validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"], :message => "The content type is incorrect"
  validates_attachment_size :image ,:in => 0.megabytes..2.megabytes, :message => "the file is too large"
  
  field :contributor_ids,          type: Array, default:[]
  field :title,				             type: String
  field :title_id,            type: BSON::ObjectId
  field :title_contributor_id,      type: BSON::ObjectId
  field :title_status,              type: Integer, default: 4
  field :content,		                 type: String
  field :content_id,             type: BSON::ObjectId
  field :content_contributor_id,         type: BSON::ObjectId
  field :content_status,                 type: Integer, default:4
  field :title_spam_contributor_ids,        type: Hash, default: {}
  field :content_spam_contributor_ids,    		 type: Hash, default: {}
  field :creator_id,			         type: BSON::ObjectId
  field :creator_name,                   type: String
  field :locked,				         type: Boolean, default: false
  field :accept_donations,               type: Boolean, default: true
  field :total_donations,	             type: Float, default: 0.0
  field :views_type,                     type: Integer, default:0
  field :views, 				         type: Array, default: [{}]
  field :send_to_front,		             type: Integer, default: 0
  field :link,                     		 type: String
  field :link_id,                 type: BSON::ObjectId
  field :link_contributor_id,            type: BSON::ObjectId
  field :link_title,               		 type: String
  field :link_title_contributor_id,    type: BSON::ObjectId
  field :link_title_status,            type: Integer, default: 4
  field :link_status,              		 type: Integer, default:4
  field :link_spam_contributor_ids,       		 type: Hash, default: {}
  field :image_spam_contributor_ids,      		 type: Hash, default: {}
  field :image_contributor_id,           type: BSON::ObjectId
  field :image_url,                      type: String
  field :image_status,                   type: Integer, default:4
  field :image_id,                  type: BSON::ObjectId
  field :last_updated_by,	             type: Hash
  field :status,                          type: Integer, default: 1
  field :locker_reputation,             type: Float, default: 0.0
  ## a combined hash field 
  ## key -> user id.
  ## value -> total count of spam contributed by the user for this element.
  field :spam_contributor_ids,          type: Hash, default: {}
  
  def self.spam_contributor_attributes
    return ["title_spam_contributor_ids","content_spam_contributor_ids","image_spam_contributor_ids","link_spam_contributor_ids"]
  end




  ## @used_in => self.assign_attributes
  ## @param[Hash] options : passed in from the self.assign_attributes arguments.
  ## @param[Element] _element : the element in question.
  ## @param[Array] attributes_to_persist : the list of attributes to persist.
  ## @return[nil]
  ## returns unless "image_id"  not present in the options.
  ## returns unless the current image_id, is the same as the existing image id.
  ## since the only thing provided in the options is the image_id, it is considered that the image is nil, and the image is set as such.
  def self.delete_image(options,_element,attributes_to_persist)
    if (options["image_id"] == _element.image_id.to_s) 
      _element.image.clear
      _element.image_status = 4
      attributes_to_persist = attributes_to_persist + IMAGE_ATTRIBUTES + ["image","image_status"]
    end
    return attributes_to_persist,_element
  end


  
  def self.assign_attributes(options,_element,current_user)

      ##CONTAINS AN ARRAY OF ATTRIBUTE NAMES TO BE CREATED/UPDATED.
      ## at each stage we first "send" the attribute value to the element
      ## then we add the attribute name to the attributes to persist.
      ## because in mongoid_versioned_update we need to know which attributes are to be persisted.
      ## and the element instance should already have those values set on it, for versioned_update to work properly
      attributes_to_persist = []


      ##delete the image if necessary.
      attributes_to_persist,_element = self.delete_image(options,_element,attributes_to_persist)

      

      ##so if the attribute is "image" and its value is nil.
      options.each do |attr_name,value|
                
          if ((CHANGABLES + ["image","icon"] + IMAGE_ATTRIBUTES).include?attr_name) && !value.nil?

            if _element.send("#{attr_name}").blank?
              #puts "no current value for this attribute: #{attr_name}"
              #puts "here are all teh image attributes"
              
              if current_user.can_add?
                  _element.send("#{attr_name}=",value)
                  
                  if assign_attribute_id(_element,attr_name)
                    attributes_to_persist << attr_name + "_id"
                  end
                  attributes_to_persist << attr_name
                  attributes_to_persist = self.assign_attribute_contributor_id_and_status(attr_name,current_user,_element,attributes_to_persist)
                  
              else
                puts "you do not have enough permissions to add"
                _element.errors.add(attr_name,"you do not have a sufficiently high reputation to add that field")
              end                          
            else
              puts "came to user.can_change with attr name: #{attr_name}"
              if current_user.can_change?(_element,attr_name)  
                if attr_has_changed?(attr_name,value,_element)
                  _element.send("#{attr_name}=",value)
                  _element = assign_attribute_id(_element,attr_name)
                  attributes_to_persist << attr_name
                  attributes_to_persist = self.assign_attribute_contributor_id_and_status(attr_name,current_user,_element,attributes_to_persist)
                  #puts "successfully changed #{attr_name}"
                else
                  #puts "attribute #{attr_name} has not changed"
                end
              else
                #puts "you do not have enough permissions to change"
                _element.errors.add(attr_name,"you do not have a sufficiently high reputation to add that field")
              end 

            end    

          end

      end

      #puts _element.image_crop_x
      #puts _element.image_crop_y
      #puts _element.image_crop_h
      #puts _element.image_crop_w


      if attributes_to_persist.size > 0
        _element.contributor_ids << [current_user.id.to_s,Time.now.to_i]
      end
      
      #puts "element attributes BEFORE SPAM:"
      #puts JSON.pretty_generate(_element.attributes)

      ##time to invoke the spam class.
      s = Spam.new
      s.element = _element
      s.attrs = options
      s.usr = current_user
      s.attributes_to_persist = attributes_to_persist
      s.add_spam
      _element = s.element
      attributes_to_persist =  s.attributes_to_persist
     
      ##finish spam addition.

      if _element.new_record?
        _element.creator_id = current_user.id.to_s
      end

      

      return attributes_to_persist,_element
  end


  def self.id_is_valid?(options={})

    if (options[:_id].nil? || !(BSON::ObjectId.legal?(options[:_id].to_s)))
      
      return false
    else
      
      return true
    end
  end

  ##persists the provided element instance.
  def self.persist(attributes_to_persist,_element)
      
      if !(attributes_to_persist.empty?)
      
        if _element.version == 0
         
          _element.versioned_create
         
                 
        else
            ##handling attributes for image update.
            ##image is nil on the element.
            ##instead the element contains the image attributes.
            ##these are added to the attributes to persist.
            ##we remove image only at this point, because it is needed earlier where we need to set image_contributor_id.

            if(attributes_to_persist.include? "image")
              attributes_to_persist.delete("image")
              attributes_to_persist = attributes_to_persist + IMAGE_ATTRIBUTES
            end
            attributes_to_persist << "contributor_ids"
            
            #puts "element attributes BEFORE versioned updated"
            #puts JSON.pretty_generate(_element.attributes)

            _element.versioned_update(Hash[attributes_to_persist.map{|c| c = [c,nil]}])

            
           
            #puts "element attributes after versioned updated"
            #puts JSON.pretty_generate(_element.attributes)

        end

      else

        _element.errors.add("_id","there are no attributes that have changed")

      end

      #puts "these are the element attributes, after save or update."
      #puts _element.attributes.to_s

      return _element

  end
  
  ##@param : attr_name -> the name of the attribute that is being checked
  ##@param : new_value -> the value being provided in the params for this attribute
  ##@param : instance -> the model instance being updated.
  ##@return : true if the values are different, false otherwise.
  def self.attr_has_changed?(attr_name,new_value,instance)
    return (instance.send("#{attr_name}") != new_value) ? true : false
  end
 

  ##the ASSUMPTIONS MADE ARE
  ##marking as spam by someone who can change the element, removes the content, and the contributor.
  ##only the guy who contributed the content can change the content, and so can mods / the person who created the corner or unit.
  ##so there is no notion of changing the contributor. 
  ##every single thing can have only one contributor, if anyone else changes anything, then he gets added to the corner contributors, and nowhere else.
  def self.assign_attribute_contributor_id_and_status(attribute,current_user,instance,changed_attribute_keys)
    
    (CHANGABLES_FOR_CONTRIBUTOR_ID).each do |stub|

      if attribute=~/#{stub}/
          
          if instance.send("#{stub}_contributor_id").nil?
            instance.send("#{stub}_contributor_id=",current_user.id)
            changed_attribute_keys << "#{stub}_contributor_id"
          else
            changed_attribute_keys << "contributor_ids"
          end
            
           ##we have to add the current user id into the array.

           ##instance.contributor_ids[current_user.id.to_s] = Time.now.to_i
           add_user_to_contributors = true
          ##we have to set the status for eg: title_status, image_status, link_status, and content_status
          attr_status = current_user.set_element_status(self,stub)
          
          ##so suppose i send a spam then this will be set to nothing.
          ##so then i have to set the status
          instance.send("#{stub}_status=",attr_status)
          #puts "instance send is: #{stub}_status"
          #puts "#{attr_status}"
          changed_attribute_keys << "#{stub}_status"

      end

    end

    ##we only add the user to the contributor ids if there is a single attribute
    

    return changed_attribute_keys

  end

  ## @param[Element] _element: the element instance being persisted.
  ## @param[String] attr_name: the name of the attribute that was either set or updated on the element instance.
  ## sets the 'attribute'_id for the persisted attribute.
  ## @example: if the title is being updated or set, we will assign 'title_id' => BSON::ObjectId.new
  ## These id's are necessary for spam assignement.
  ## @return[Boolean] : true if the attribute is amongst the CHANGABLES_FOR_ATTRIBUTE_ID and it is successfully set on the element, false otherwise.
  def self.assign_attribute_id(_element,attr_name)
    if(CHANGABLES_FOR_ATTRIBUTE_ID.include?(attr_name))
        _element.send("#{attr_name}_id=",BSON::ObjectId.new)
    else
        false
    end
  end





=begin
METHODS REFERENCED FROM 
=end
  
  def is_locked?
    return locked
  end

  def has_content?
  	return content!=""
  end

  def has_title?
  	return title!=""
  end
  
  ##the image needs moderation.
  def image_pending_moderation?
  	return image_status == 0
  end

  ##the link needs moderation
  def link_pending_moderation?
  	return link_status == 0
  end

  ##the content needs moderation
  def content_pending_moderation?
  	return content_status == 0
  end

  def title_pending_moderation?
    return title_status == 0
  end

  ##the whole element needs moderation.
  def element_pending_moderation?
    return status == 0
  end

  def any_moderation?
    return (image_pending_moderation? || link_pending_moderation? || content_pending_moderation? || element_pending_moderation? || title_pending_moderation?)
  end

  def has_image?
  	return image_status!=4
  end

  def has_content?
  	return content_status!=4
  end	

  def has_link?
  	return link_status!=4
  end	

  def has_title?
    return title_status!=4
  end

  def has_clean_image?
  	return image_status == 1 && image_spam_contributor_ids.size < IMAGE_SPAM_COUNT
  end

  def has_clean_content?
  	return content_status == 1 && content_spam_contributor_ids.size < CONTENT_SPAM_COUNT
  end

  def has_clean_link?
  	return link_status == 1 && link_spam_contributor_ids.size < LINK_SPAM_COUNT
  end

  def has_clean_title?
    return title_status == 1 && title_spam_contributor_ids.size < TITLE_SPAM_COUNT
  end


  def has_entity?(type)
    if type == "image"
      return has_image?
    elsif type == "link"
      return has_link?
    elsif type == "content"
      return has_content?
    elsif type == "title"
      return has_title?
    end
  end


  def has_clean_entity?(type)
    #Rails.logger.debug("checking has clean entity" + type)
    if type == "image"
      #Rails.logger.debug("image returns: " + has_clean_image?.to_s)
      return has_clean_image?
    elsif type == "link"
      return has_clean_link?
    elsif type == "content"
      return has_clean_content?
    elsif type == "title"
      return has_clean_title?
    end
  end

  def entity_pending_moderation?(type)
    #Rails.logger.debug("checking entity pending moderation:" + type)
    if type == "image"
      #Rails.logger.debug("image returns:" + image_pending_moderation?.to_s)
      return image_pending_moderation?
    elsif type == "link"
      #Rails.logger.debug("link returns:" + link_pending_moderation?.to_s)
      return link_pending_moderation?
    elsif type == "content"
      #Rails.logger.debug("content pending moderation:" + content_pending_moderation?.to_s)
      return content_pending_moderation?
    elsif type == "title"
      #Rails.logger.debug("title pending moderation:" + title_pending_moderation?.to_s)
      return title_pending_moderation?
    end

  end

  def add_new_or_upload_entity(type,intro=false)
    if type == "image"
      return "Upload An Image"
    elsif type == "link"
      return "Add A Link"
    elsif type == "content"
      return intro==true ? "Add An Intro" : "Add More Info"
    elsif type == "title"
      return true
    end
  end

  def change_entity(type,intro=false)
    if type == "image"
      return "Change Image"
    elsif type == "link"
      return "Change Link"
    elsif type == "content"
      return true
    elsif type == "title"
      return true
    end
  end


  def get_entity_contributor_id(type)
    if type == "image"
      return image_contributor_id
    elsif type == "link"
      return link_contributor_id
    elsif type == "content"
      return content_contributor_id
    elsif type == "title"
      return title_contributor_id
    end
  end


  ##returns a hash of {user_id => pending_moderation_for_type}
  def pending_moderation_user_ids
    
    ret_hash = {}

    if image_pending_moderation?
      ret_hash["image"] = image_contributor_id
    end

    if link_pending_moderation?
      ret_hash["link"] = link_contributor_id
    end

    if content_pending_moderation?
      ret_hash["content"] = content_contributor_id
    end

    if title_pending_moderation?
      ret_hash["title"] = title_contributor_id
    end

    return ret_hash

  end

  def set_image_from_url(url_value)
    if url_value.nil? || url_value.empty?
    
    else
      url_value = validate_image_url(url_value)
      self.image = URI.parse(url_value)
      @image_url = url_value
    end
  end

 
  def is_a_corner?
    return _type == "Corner"
  end

  def is_a_unit?
    return _type == "Unit"
  end

  ## @used_in: edit_image.html.erb
  ## @return[String] corner_id : returns the corner id of the element.
  ## if it is a corner, then just returns its id, otherwise if its  a unit
  ## then returns the units corner_id.  
  def get_corner_id
    if is_a_corner?
      self.id.to_s
    elsif is_a_unit?
      self.corner_id.to_s
    end
  end
  

  ## @used_in: corners_helper.rb
  ## Element.spam_contributor_attributes returns the names of the attribute_spam_contributor_ids fields. for now these are:
  ## image_spam_contributor_ids, title_spam...blah blah.
  ## so we run a keep_if on this array, provided that the previous_changes contains this key, and the last_element in the value at this key in the previous_changes hash is also not blank, indicating htat this field was changed in the last commit.
  ## @return[Array] : array of {attribute}_spam_contributor_ids field names that were changed in the last commit.
  def spam_attributes_added   
    Element.spam_contributor_attributes.keep_if{|sca|
      (!self.previous_changes[sca].nil?  && !self.previous_changes[sca][1].blank?)
    }
  end

  ## @used_in: corners_helper.rb
  ## @return[Integer] : the total number of times all the attributes of this element have been marked as spam. If there are no spam for this element, returns 0 otherwise the total number of spam count.
  def get_total_spam_count
    if a = self.spam_contributor_ids.values.inject(:+)
      a
    else
      0
    end
  end

  ## @used_in: corners/_spam_icon.js.erb
  ## this is used to decide whether to make the spam_icon in the spam_table red, based on whether the current user has marked this attribute as spam.
  ## @param[String] attribute_marked_for_spam: the name of the attribute that we are checking to see if it has the current user id in its spam.
  ## @param[User] curr_usr : the current user.
  ## @return[Boolean] : if the current user id is present in this attributes spam_contributor_ids then true, otherwise false.
  def attribute_marked_for_spam_contains_curr_usr(attribute_marked_for_spam,curr_usr)
    return false unless curr_usr
    if self.send(attribute_marked_for_spam + "_spam_contributor_ids")[curr_usr.id.to_s]
      return true
    else
      return false
    end
  end

  ## @used_in: corners_helper.rb
  ## @return[String] attribute_name : the name of the attribute that was marked as spam or nil, if none was marked.
  ##then we map it to remove the _spam_contributor_ids from the field name, so that we are left with either image,title,link, or content.
  ##we take only the first element, because only one element can be marked for spam at a time.
  ##then we return the zeroeth element of the array.
  def get_spammed_attribute
    attribute_marked_for_spam = self.spam_attributes_added.map.each_with_index{|c,i|
         if i == 0
          c = c.gsub(/_spam_contributor_ids/){|m| ""}
         end
      }.flatten
    attribute_marked_for_spam[0]
  end

  ## @used_in:  corners/_card_reveal.html.erb
  ## @params[String] type : either image/title/content/link
  ## @returns[Boolean] true if there is spam for this attribute otherwise false.
  def has_spam(type)
    self.send("#{type}_spam_contributor_ids").values.compact.size
  end


  ## @used_in: corners/_spam_icon.html.erb
  ## @params[String] type: either image/title/content/link
  ## @returns[Boolean] : true if existing spam count for this type is already equal to the max spam count, false otherwise.
  def has_max_permissible_spam(type)
    return has_spam(type) == Spam.max_spam_count
  end

  ## @used_in: elements_controller.rb
  ## @returns[Element] : returns the element which has the image_id of the current element, or nil if no such element exists.
  def image_id_matches?
    return unless self.image_id
    Element.where(:id => self.id, :image_id => self.image_id).first
  end

  private 

  ##removes whitespaces from non nil textual attributes.
  ##called before validation. specified as a callback.
  def remove_whitespaces
    if !title.nil? 
      title.strip! 
    end
    if !content.nil?
     content.strip!
    end
    if !link.nil?
     link.strip!
    end
    if !link_title.nil?
     link.strip!
    end
    if !image_url.nil?
     image_url.strip!
    end
  end

end
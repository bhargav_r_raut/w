class List
  include Virtus.model

  ##element_id -> the id of the element for which we want spam
  attribute :element_id, String
  
  ##spam_for_attribute -> the attribute for which we want the spam on the given element
  attribute :spam_for_attribute, String
  
  ##upto_time -> the timestamp from which we want the spam, eg if we provide 12.30 pm on 10th june, then we get all spam which was set before that
  attribute :upto_time, Integer, :default => Time.now.to_i
  
  ##number -> number of users upto that particular time.
  attribute :number, Integer, :default => 10
  
  ##the generated users list.
  attribute :users_list, Array, :default => []
  
  ## @used_in : list_controller #element_spam_list
  ## find the element and from the attribute_spam user ids hash take those keys which have a value upto the self.upto_time and take only a number of keys equal to -> self.number.
  ## build the users list using these ids, and just set self.users_list, to an array of these ids.
  def get_el_attribute_spam_users
  	if e = Element.where(:id => self.element_id).only("#{self.spam_for_attribute}_spam_contributor_ids").first
   		build_users_list(e.send("#{self.spam_for_attribute}_spam_contributor_ids").select{|k,v| v <= self.upto_time}.keys[0..number])
 	  end
  end

  def build_users_list(user_ids)
    self.users_list = user_ids.map { |e|
      begin
        User.find(e)
      rescue
        nil
      end
    }.compact
  end

end
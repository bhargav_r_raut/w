class Corner < Element

  field :score,                         type: Float, default: 1.0
 
  def self.build(options={},current_user)
    if id_is_valid?(options)
      _element = self.find_or_initialize_by(Hash["_id",options.fetch("_id").to_s])
      attributes_to_persist,_element = assign_attributes(options,_element,current_user)
      _element = persist(attributes_to_persist,_element)
    else
      _element = self.new
      _element.errors.add(:_id,"the provided id is invalid")
    end
    return _element
  end

end

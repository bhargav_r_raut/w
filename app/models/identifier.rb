class Identifier
  include Mongoid::Document

  field :corner_identifier, 			type: String
  field :intro_identifier,				type: String
  field :section_identifiers,			type: String

end

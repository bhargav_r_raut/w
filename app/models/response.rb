class Response
  include Virtus.model
  attribute :corner,     Corner
  attribute :units,    Array[Unit]
  attribute :errors,      Hash
  attribute :messages, Hash
  attribute :creator, User
end
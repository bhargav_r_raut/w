class Image
  include Mongoid::Document
  include Mongoid::Paperclip

  field :player, type: String
  field :best_asset, type: String
  field :image_url, type: String

  validates :player, presence: {message:"need a player"}
  validates :best_asset, length: {minimum: 10, message:"shoule be good at penalties"}

  belongs_to :test, :inverse_of => :images
  has_mongoid_attached_file :file, styles: { thumb: ["64x64#", :jpg] }
  validates_attachment_content_type :file, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"], :message => "The content type is incorrect"

  #validate :file_dimensions

  private

  def file_dimensions(width = 680, height = 540)
    dimensions = Paperclip::Geometry.from_file(file.queued_for_write[:original].path)
    unless dimensions.width == width && dimensions.height == height
      errors.add :file, "Width must be #{width}px and height must be #{height}px"
    end
  end

  def image_url=(url_value)
    if url_value.nil? || url_value.empty?
    
    else
      url_value = validate_image_url(url_value)
      
      self.file = URI.parse(url_value)
      # Assuming url_value is http://example.com/photos/face.png
      # avatar_file_name == "face.png"
      # avatar_content_type == "image/png"
      @file = url_value
    
    end
  end

end

class Spam
	
	attr_accessor :element,:attrs, :usr, :spam_types_updated, :attributes_to_persist
	
	## 
	def self.max_spam_count
		return 1
	end

	## => expects the spam hash in the incoming attributes to have the following structure
	## => {:image => image_id, :title => title_id, :content => :content_id, :link => link_id}
	## => the hash can be empty or contain any of the above attributes.
	## => the method checks if the existing id for each of the above attributes is the same as the value(image/title/content/link/ id.) provided above.
	## => if yes, it will increment the spam count.
	## => if no, it will not do anything.
	## => @return[Hash] attributes_to_persist
	def add_spam
		if !@attrs["spam"].nil?
			##now we have
			##we need to check if this is the current spam id , and if yes then
			@attrs["spam"].keys.each do |type|
				type_id = type + "_id"
				type_spam_contributor_ids_field = type + "_spam_contributor_ids"
				type_spam_contributor_ids = @element.send(type_spam_contributor_ids_field)
				if @element.send(type_id).nil?
					##we cannot assign spam, because there is no id for this type, which is strange, because it means that this type(image/link/title/content) does not exist for this element.
				elsif @element.send(type_id).to_s != @attrs["spam"][type]
					##we cannot assign spam, since the type id has changed.
				
				else
					##if the user has already marked this item as spam in the past, then unmark it.
					if type_spam_contributor_ids[@usr.id.to_s]
						type_spam_contributor_ids[@usr.id.to_s] = nil
						decrement_total_spam_contributors
						@element.send(type_spam_contributor_ids_field + "=",type_spam_contributor_ids)
						@attributes_to_persist << type_spam_contributor_ids_field
					##if the user hasnt marked this as spam, then add his id to the respective hash, and set the value as the current time.
					else
						
						if @element.has_spam(type) == Spam.max_spam_count
						else
							type_spam_contributor_ids[@usr.id.to_s] = Time.now.to_i
							increment_total_spam_contributors
							@element.send(type_spam_contributor_ids_field + "=",type_spam_contributor_ids)
							@attributes_to_persist << type_spam_contributor_ids_field
						end
					end
					
				end
			end 
		end
	end

	def increment_total_spam_contributors
		if current_count = @element.spam_contributor_ids[@usr.id.to_s]
			@element.spam_contributor_ids[@usr.id.to_s] += 1
		else
			@element.spam_contributor_ids[@usr.id.to_s] = 1
		end
		@attributes_to_persist << "spam_contributor_ids"
	end

	def decrement_total_spam_contributors
		if current_count = @element.spam_contributor_ids[@usr.id.to_s]
			if current_count == 1
				@element.spam_contributor_ids[@usr.id.to_s] = nil
			else
				@element.spam_contributor_ids[@usr.id.to_s] -= 1
			end
		end
		@attributes_to_persist << "spam_contributor_ids"
	end

end

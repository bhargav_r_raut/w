class Unit < Element
    
  validates :icon, numericality: {only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 2}, unless: :only_image_being_persisted

  validate :legal_corner_id_provided
  validate :icon_and_title_present, unless: :only_image_being_persisted
  validate :corner_id_valid, unless: :only_image_being_persisted
  
  ##update the corner to reflect that it was updated by the author of this unit.
  ##this should be a non-versioned updated.
  after_save :corner_updated_at_and_by

  ##FIELDS DIFFERENT FROM THE CORNER.
  
  field :icon,                             type: Integer
  field :intro_content_request_ids,      type: Array, default: []
  field :corner_id,                     type: BSON::ObjectId
  field :score,                         type: Float, default: 0.0


  ##first ensures that there is a corner id, otherwise will add to the errors.
  def self.build(options={},current_user)
    if id_is_valid?(options)
     
    
      attributes_to_persist,_element = assign_attributes(options,current_user)
      
      ##if the element has only image attributes, then skip the corner id exists callback

      _element = persist(attributes_to_persist,_element)
      
    else
      _element = self.new
      _element.errors.add(:_id,"the provided id is not valid")
    end
    return _element
  end

  ##add the corner id and the icon
  def self.assign_attributes(options,current_user)
    _element = nil
    begin
    _element = Unit.find(options.fetch("_id").to_s)
    #puts "found element"
    #puts _element.attributes.to_s
    rescue
    #puts "triggered rescue."
    _element = Unit.new({"_id" => options.fetch("_id").to_s})
    end
    #puts "is it a new record."
    #puts _element.new_record?
    #puts "at this stage check icon is:."
    #puts _element.icon.to_s
    #puts "it is expected to be nil."
    attributes_to_persist,_element = super(options,_element, current_user)
    ##if it already doesnt have a corner id, then assing the new one.
    if _element.corner_id.nil?
      _element.corner_id = options[:corner_id]
    else
      if _element.corner_id != options[:corner_id]
        _element.errors.add("corner_id","the corner id of this unit does not match the one provided earlier")
      end
    end

    if _element.icon.nil? && current_user.can_add?
      _element.icon = options[:icon]
    end

    ##if the user can change the icon?
    return attributes_to_persist,_element
  end

  def only_has_image?
    if icon.nil? && title.nil? && content.nil? && link.nil? && !image.nil?
      true
    end
  end

  def has_an_icon?
    return !icon.nil?
  end

  def has_sun_icon?
    return icon == 0
  end

  def has_rain_icon?
    return icon == 1
  end 

  def has_storm_icon?
    return icon == 2
  end

  def has_not_sun_icon?
    return icon!=0
  end

  def has_not_rain_icon?
    return icon!=1
  end

  def has_not_storm_icon?
    return icon!=2
  end

  protected

  ##after save.
  def corner_updated_at_and_by

    ##provided that there is a corner id, and the contributor_ids of the unit are not empty.
    ##bypasses versioning, and also disables upsert.
    if !self.corner_id.nil? && !self.contributor_ids.empty?
      Corner.versioned_upsert_one({"_id" => self.corner_id},{"$push" => {"contributor_ids" => self.contributor_ids.last}},false,false,true)
    end

  end



  def only_image_being_persisted

    ##if the only attributes being persisted are image attributes
    #puts "entered the only image being processed thing."
    attributes.each do |attrib,value|
      if (CHANGABLES + ["icon"]).include? attrib 
        if !value.nil?
          return false
        end
      end
    end
    return true
  end

  def corner_id_valid
    
    if !corner_id.nil?
      begin
        Corner.find(corner_id)
      rescue => e
        
        errors.add(:corner_id,"this corner id does not exist")
      end
    end
  end

  def legal_corner_id_provided
    if corner_id.nil? || !Element.id_is_valid?({:_id => corner_id})
      errors.add(:corner_id, "unit does not have a corner id")
    end
  end

  def icon_and_title_present
    puts "came to validate icon and title."
    if title.nil?
      puts "title is nil"
      errors.add(:title,"unit is missing a title")
    elsif icon.nil?
      puts "icon is nil"
      errors.add(:icon,"unit is missing an icon")
    end
  end

end

var invalid_field_callback_function = function(){
  $("#new_user, #edit_user").find("[class=virgin]").each(function(index){
    $(this).prop("disabled",true);
    
  });
}

var valid_field_callback_function = function(){
  
  $("#new_user, #edit_user").find("input").each(function(index){
    $(this).prop("disabled",false);
    var label =  $(this)
            .closest("form")
            .find("label[for='" + $(this).attr("id") + "']");
    label.removeAttr("data-error");
    var validator = $( "#new_user, #edit_user" ).validate();
    validator.resetForm();
  });

  
}

var registrations_on_load = function(){
  
  if(should_js_run("registration") === false){
    //console.log("returned from registrations");
    return;
  }

	$.validator.setDefaults({
	
    errorClass: 'invalid',
    validClass: "valid",
    showErrors:function(errorMap,errorList){
        this.defaultShowErrors();
        if(!jQuery.isEmptyObject(errorList)){
          invalid_field_callback_function();  
        }
        else{
          valid_field_callback_function();
        }
    },
    errorPlacement: function (error, element) {
  
        var q = $(element)
            .closest("form")
            .find("label[for='" + element.attr("id") + "']")
            .attr('data-error', error.text());
  
    }
	});

	$("#new_user").validate({
      
      onkeyup: function(element) { 
        $(element).valid();
      },
	    rules: {
          "user[password_confirmation]":{
            equalTo: "#user_password"
          },      
	        "user[email]":
                {
                    email: true,
                    "remote":
                    {
                      url: '/users/email_exists',
                      type: "get",
                      data:
                      {
                          email: function()
                          {
                              return $('#new_user :input[name="user[email]"]').val();
                          }
                      }
                    }
                }
	    },
	    messages: {
	        "user[email]": {
	            remote: jQuery.validator.format("That email address is already registered")  
	        },
          "user[password_confirmation]": "The passwords don't match"
	    }
	});

  $("#edit_user").validate({
      
      onkeyup: function(element) { 
        $(element).valid();
      },
      rules: {
          "user[password_confirmation]":{
            equalTo: "#user_password"
          },      
          "user[email]":
                {
                    email: true,
                    "remote":
                    {
                      url: '/users/email_matches_current',
                      type: "get",
                      data:
                      {
                          email: function()
                          {
                              return $('#edit_user :input[name="user[email]"]').val();
                          }
                      }
                    }
                }
      },
      messages: {
          "user[email]": {
              remote: jQuery.validator.format("This does not match your registered email")  
          },
          "user[password_confirmation]": "The passwords don't match"
      }
  });

}

/**
@focusout function.
**/
$(document).on('focusout','#user_email,#user_password,#confirmpassword',function(event){
    if(should_js_run("registration") === false){
      return;
    }
    
      if(!$(this).val()){
        $(this).removeAttr("aria-invalid");
        $(this).attr("class","");
        var label =  $(this)
              .closest("form")
              .find("label[for='" + $(this).attr("id") + "']");
        label.removeAttr("data-error");
        label.attr("class","");
     
    }
});


$(document).on('click','input',function(event){
 
  var self_id = $(this).attr("id");

  $("input").each(function(index){
    
    if(typeof $(this).attr("class")!= 'undefined'){

      if($(this).attr("class").indexOf("invalid")!=-1 && self_id  != $(this).attr("id")){

          //its invalid, so clear it and make it valid.
          $(this).attr("class","virgin");
          $(this).val("");
      }

    }

  });
    

});



$(document).on('ajax:beforeSend','form.new_user', function(event, xhr, settings) {
   
    if(should_js_run("registration") === false){
      return;
    }
      var recaptcha_response = $("#g-recaptcha-response").val();
     
      if(!recaptcha_response.trim()){
        //its empty, so abort the execution of the form.
        //and show error message. as a modal.
        //console.log("came to xhr abort");
        xhr.abort();
       
        $("#captcha_image").show();
        $("#recaptcha_errors_modal").openModal();
        $("#recaptcha_failure_message").text("You haven't checked the tick box in the captcha.");
        //now show the modal
      }
    

});






$(document).ready(registrations_on_load);
$(document).on('page:load', registrations_on_load);

/****

notes:

the validation plugin has several issues, and the following workarounds were necessary:

1.if a field becomes invalid for some reason, and then the user empties it, the invalid class
as well as the error remain on the field.
for this purpose:
@focusout function: 
it ensures that if a field is empty, then firstly the aria-invalid, which gets applied to the invalid
input field by the validator is removed, and also its class is set to empty.(neither active nor invalid)
then we find the label of the input field and remove its data-error attribute, and set its class to empty, same as above.
the data-error attribute is the one that holds the error message.


2.errorPlacement is overridden in the validate options:
@Reason:
in order to use the jquery validate plugin with materializecss, we have to tell the plugin where to place the error messages. so we tell it to place the error messages in the nearest label to the said input field.

3.showErrors is overriden in the validate options:
@Reason:
showErrors is actually a callback that gets called whenever a field is validated.
whether it has errors or not. so what we do is that if it does not have errors we call our "valid_callback_function", and if it has errors we call our invalid_callbacks_function.

we want some special behaviour for invalid and valid:

@invalid: 
here we want to disable all other fields.there are some tricks with the classes.
on page load all input fields have the class "virgin". suppose that a particular field becomes invalid,
it gets the class "virgin invalid", on the other hand if there is already some other field which is valid, its class will be "virgin valid".
so when a field gets marked as invalid, all fields that have only the class "virgin" get disabled.
this leaves both valid and invalid field, still inputtable, but ensures that no other fields can be entered.

@valid:
when a field becomes valid, we first enable all input fields, (basically any virgin field, since only that could have been disabled.)
we also remove the data-error attribute from the current input fields label.(because the error does not get automatically removed by the plugin when a field becomes valid)
we also reset the form, because the plugin stupidly saves the older errors, and these pop up when trying to submit the damn thing.


4.keyup function:
validations are done on keyup, so keyup is added to the validation options.

#changes made to validate.js

1.when the submit button is clicked, the plugin calls its validate function.
inside this validate function, 

if ( validator.pendingRequest ) {
            validator.formSubmitted = true;
            
            
            return validator;
}
this change is necessary to get the form to submit, the form wasnt submitting at all before that.
the line that is changed, is that initially it was return false; and i replaced it with return validator.


PENDING:
--------
2.the ajax remote call for the email validation, is done only when the email address obeys the basic regex of emails. otherwise the call is not made.
the ajax call is modified so that in beforesend, a circle animation is show and it is removed on error or after send.

3.captcha is pending.

4.password minimum lenght, alphanumeric



*****/
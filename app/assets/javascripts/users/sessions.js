var sessions_on_load = function(){
	
	if(should_js_run("session") === false){
		//console.log("returned from sessions");
		return;
	}

	$('select').material_select();
	var ctx = $("#sign_in_chart").get(0).getContext("2d");
	/****
	dummy data.
	***/
	var data = {
    labels: ["4 am", "5 am", "6 am", "7 am", "12pm", "3 pm", "7 pm"],
	    datasets: [
	        {
	            label: "My First dataset",
	            fillColor: "rgba(220,220,220,0.2)",
	            strokeColor: "rgba(220,220,220,1)",
	            pointColor: "rgba(220,220,220,1)",
	            pointStrokeColor: "#fff",
	            pointHighlightFill: "#fff",
	            pointHighlightStroke: "rgba(220,220,220,1)",
	            data: [65, 59, 80, 81, 56, 55, 40,100,200,120],
	            tooltip_objects:[]
	        }
	    ]
	};
	/***
	end dummy data
	***/

	var myLineChart = new Chart(ctx).Line(data, {});

}
$(document).ready(sessions_on_load);
$(document).on('page:load', sessions_on_load)
;

$(document).on('change','#help',function(){
	if(should_js_run("session") === false){
		//console.log("returned from sessions");
		return;
	}
	window.location.pathname = $(this).val() 
});


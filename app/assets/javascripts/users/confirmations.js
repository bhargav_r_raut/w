var confirmations_on_load = function(){
  if(should_js_run("confirmation") === false){
    //console.log("returned from confirmations.");
    return;
  }

  $.validator.setDefaults({
	
    errorClass: 'invalid',
    validClass: "valid",
    showErrors:function(errorMap,errorList){
        this.defaultShowErrors();
        if(!jQuery.isEmptyObject(errorList)){
          invalid_field_callback_function();  
        }
        else{
          valid_field_callback_function();
        }
    },
    errorPlacement: function (error, element) {
  
        var q = $(element)
            .closest("form")
            .find("label[for='" + element.attr("id") + "']")
            .attr('data-error', error.text());
  
    }
	});

  $("#new_user").validate({
      
      onkeyup: function(element) { 
        $(element).valid();
      },
	    rules: {   
	        "user[email]":
                {
                    email: true
                    
                }
	    }
	});

}

$(document).ready(confirmations_on_load);
$(document).on('page:load', confirmations_on_load);

$(document).on('focusout','#user_email',function(event){
    if(should_js_run("confirmation") === false){
      //console.log("returned from confirmations");
      return;
    }
    if(!$(this).val()){
      $(this).removeAttr("aria-invalid");
      $(this).attr("class","");
      var label =  $(this)
            .closest("form")
            .find("label[for='" + $(this).attr("id") + "']");
      label.removeAttr("data-error");
      label.attr("class","");
    }
});
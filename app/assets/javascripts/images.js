/**
images.js
**/
//= require azure.fine-uploader.js
//= require jquery.jcrop.js
//= require load-image.all.min.js
//= require paste.js
//= require wj_upload.js


/***
var uploader = null;
$(document).ready(function(){
    console.log("the csrf token is:" + $('meta[name="csrf-token"]'));
	uploader = new qq.azure.FineUploader({
    	element: document.getElementById("uploader"),
    	request: {
            endpoint: 'https://wordjelly.blob.core.windows.net/wordjelly-container'
        },
        signature: {
            endpoint: 'azure_sas_uri'
        },
        uploadSuccess: {
            endpoint: 'azure_success',
            customHeaders: {
            	//has to get the headers here.
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr("content")
            }
        }
	});

    //setup the contenteditable pastable functionality.
    $("#paste_image_div").pastableContenteditable();

    $('#paste_image_div').on('pasteImage', function(ev, data){
    var blobUrl = URL.createObjectURL(data.blob);
       // $('<div class="result">image: ' + data.width + ' x ' + data.height + '<img src="' + data.dataURL +'" ><a href="' + blobUrl + '">' + blobUrl + '</div>').insertAfter(this);
        //var img = '<img src="' + data.dataURL + '">';
        //$("#result").append(img);
        //uploader.addFiles([data.blob]);
        //$("#actions").show();
        paste_crop_handler(data.blob);
    }).on('pasteImageError', function(ev, data){
        alert('Oops: ' + data.message);
        if(data.url){
          alert('But we got its url anyway:' + data.url)
        }
    });
    
});


var result = $('#result')
  var exifNode = $('#exif')
  var thumbNode = $('#thumbnail')
  var actionsNode = $('#actions')
  var currentFile
  var coordinates


function paste_crop_handler(blob){
    var options = {
      maxWidth: result.width(),
      pixelRatio: window.devicePixelRatio,
      downsamplingRatio: 0.5,
      canvas: true,
      orientation: true
    }
    if (!blob) {
      return
    }
    exifNode.hide()
    thumbNode.hide()
    displayImage(blob, options)
} 

function updateResults (img, data) {
    
    var content
    if (!(img.src || img instanceof HTMLCanvasElement)) {
      content = $('<span>Loading image file failed</span>')
    } else {
      content = $('<a target="_blank">').append(img)
        .attr('download', currentFile.name)
        .attr('href', img.src || img.toDataURL())
    }
   
    $("#result").children().replaceWith(content)
   
    if (img.getContext) {
      $('#actions').show()
    }
    if (data && data.exif) {
      displayExifData(data.exif)
    }
  }

  function displayExifData (exif) {
    var thumbnail = exif.get('Thumbnail')
    var tags = exif.getAll()
    var table = exifNode.find('table').empty()
    var row = $('<tr></tr>')
    var cell = $('<td></td>')
    var prop
    if (thumbnail) {
      thumbNode.empty()
      loadImage(thumbnail, function (img) {
        thumbNode.append(img).show()
      }, {orientation: exif.get('Orientation')})
    }
    for (prop in tags) {
      if (tags.hasOwnProperty(prop)) {
        table.append(
          row.clone()
            .append(cell.clone().text(prop))
            .append(cell.clone().text(tags[prop]))
        )
      }
    }
    exifNode.show()
  }

  function displayImage (file, options) {
   
    currentFile = file
    if (!loadImage(
        file,
        updateResults,
        options
      )) {
        console.log("load image faled");
      result.children().replaceWith(
        $('<span>' +
          'Your browser does not support the URL or FileReader API.' +
          '</span>')
      )
    }
  }



$(document).on("click","#upload_cropped",function(event){
    uploader.addFiles([$("canvas")[0]]);
});

**/


// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require papercrop
//= require materialize-sprockets
//= require jquery_ujs
//= require jquery.remotipart
//= require turbolinks
//= require jquery-ui
//= require underscore
//= require chrome_scroll
//= require jquery.extensions.js
//= require jquery.validate.js
//= require Chart.js
//= require clicky.js
//= require form_csrf_protection.js
//= require should_js_run.js
//= require spinner.js
//= require moderate.js
//= require open_new_tab.js
//= require jquery.tooltipster.min.js
//= require moment
//= require svg_to_css.js
//= require contenteditable.js
//= require ajax_call.js
//= require cards_modification.js
//= require leanModal_modification.js
//= require you_need_to_sign_in.js
//= require corners.js
//= require images.js
//= require users/users_list_modal.js



//need to add the azure files

var app_on_load = function(){
	///$(".users_list_activator").each(function(index){
	// 	apply_users_list_modal($(this).attr("id"));
	//});
}


$(document).on('turbolinks:load',app_on_load);

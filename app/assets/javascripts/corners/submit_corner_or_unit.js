/***
ARGUMENTS:
====
validate -> boolean -> whether the form should be validated or not.
e -> click_event -> the event that triggered the build form.
====

WORKING:
====
It will take every jquery element that has data-corner-id equal to the data-corner-id attribute of whatever was the target of the click-event.
It will build the form by iterating all these elements.
====

on click upload image if the user is not signed in, 
build the form, without the validations.
then submit the form.
in the elements, controller there will be a before option which checks if the user is signed in.
if not signed in , the params are dumped into the session.
after sign in, he is redirected to the corner create action if there are corners / units pending in his session.
these are used to create the corner,
corner creation, has to be lazy and non-nasty, i.e it should create any field that is not invalid.
****/
var build_form = function(validate,e){
	
	var data_corner_id = $(e.target).attr("data-corner-id");
	var params = {"units":[],"corner":null};
	var commit_hash = {};
	var valid = true;
	
	$('.dirty[data-corner-id="'+ data_corner_id +'"]').each(function(index){
													
		var el_id = $(this).attr("data-element-id");
		var corner_id = $(this).attr("data-corner-id");
		var belongs_to_c = belongs_to_corner($(this));
		var version = get_version($(this),belongs_to_c ? "corner" : "unit");
		
		if(!(el_id in commit_hash)){
			commit_hash[el_id] = {"_id" : el_id};
			commit_hash[el_id]["version"] = version;
			if (corner_id){
				commit_hash[el_id]["corner_id"] = corner_id;
			}
		}

		if($(this).hasClass("editable_links")){
			commit_hash[el_id]["link_title"]  = $(this).attr("data-link-title");
			commit_hash[el_id]["link"]  = $(this).attr("data-link");
		}
		else if($(this).hasClass("content")){
		
			commit_hash[el_id]["content"] = $(this).text().trim();
		}
		else if($(this).hasClass("title")){
		
			commit_hash[el_id]["title"] = $(this).text().trim();
		}
		else if($(this).hasClass("weather_holder")){
			try{

				commit_hash[el_id]["icon"] = parseInt($(this).attr("data-icon-set-id"));
			}
			catch(err){

			}
		}	
		else if($(this).hasClass("spam_marker")){
				if(!commit_hash[el_id][SPAM]){
					commit_hash[el_id][SPAM] = {};
				}
				var spam_type = $(this).attr("data-spam-type");
				var type_id = $(this).attr("data-type-id");
				commit_hash[el_id][SPAM][spam_type] = type_id;
		}
		//this part we are adding for deleting the image, we just pass in the image id, and nothing else, into the request.
		else if($(this).hasClass("delete_image")){
			commit_hash[el_id]["image_id"] = $(this).attr("data-type-id");
		}
		else if($(this).hasClass("crop-attribute")){
			commit_hash[el_id][$(this).attr("id")] = $(this).val();
		}

		
	});

	var type;
	
	for(id in commit_hash){
		type = $("#" + id + "_type").attr("data-is-a-corner") == 0 ? "corner" : "unit";
		if(type == "corner"){
			params["corner"] = commit_hash[id];
		}
		else{

			params["units"].push(commit_hash[id]);
		}
		
	}

	if(validate){
		if(validate_corner_title(params,e)){
			for(id in commit_hash){
				if(valid){
					valid = validate_unit(id,e);
				}
			}
		}
		else{
			valid = false;
		}	
	}	

	return valid === true ? params : false;

}




//the only function that should be called to post the form.
var submit_corner = function(event){	
	
	console.log("called submit corner=========================");
	data_hash  = build_form(user_signed_in(),event);
	console.log("the data hash is:");
	console.log(JSON.stringify(data_hash,null,'\t'));

	if(data_hash && (data_hash["units"].length > 0 || data_hash["corner"])){
		return ajax_call("/elements",data_hash,"POST");
	}
	return null;
}

$(document).on('click','.publish',function(event){
	if(!sign_in_required(event)){
		submit_corner(event);
	}
});


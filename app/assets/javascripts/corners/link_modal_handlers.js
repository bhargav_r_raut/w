/*****
FUNCTIONS FOR THE ADD_A_LINK_MODAL
*****/

var store_link_and_title_values = function(options){
	$("#" + options.opener_id).attr("data-link",$(link_id).val());
	$("#" + options.opener_id).attr("data-link-title",$(link_title_id).val());
}

var clear_fields = function(){
	$(link_id).val("");
	$(link_title_id).val("");
}

/***
make the link and title whatever they were initially.
***/
var revert_link_and_title_values = function(options){
	$("#" + options.opener_id).attr("data-link",get_initial_value(options,link));
	$("#" + options.opener_id).attr("data-link-title",get_initial_value(options,link_title));
}

var set_link_icon_tooltip = function(options){
	
	var title_attr = $(link_title_id).val().length > 0 ? $(link_title_id).val() : $(link_id).val(); 
	$("#" + options.opener_id).attr("title",title_attr);
}

var set_dirty = function(options,link_or_title){
	
	if(has_initial_value(options,link_or_title)){
		if(get_initial_value(options,link_or_title) != get_new_value(options,link_or_title)){
			$("#" + options.opener_id).addClass("dirty");
			return true;	
		}
	}
	else{
		$("#" + options.opener_id).addClass("dirty");
		return true;
	}
}


var activate_link_icon = function(options){
	$("#" + options.opener_id).removeClass("grey-text teal-text text-lighten-2").addClass("amber-text");
	$("#" + options.opener_id).attr("data-defaultcolor","amber-text");
	$("#" + options.opener_id).attr("data-hovercolor","amber-text");
	var link_set = set_dirty(options,link);
	var link_title_set = set_dirty(options,link_title);
	if(link_set || link_title_set){
		submit_corner(options.event);
	}
}


var deactivate_link_icon = function(options){
	$("#" + options.opener_id).removeClass("amber-text").addClass("grey-text text-lighten-2");
	$("#" + options.opener_id).removeClass("dirty");
	$("#" + options.opener_id).attr("data-defaultcolor","grey-text");
	$("#" + options.opener_id).attr("data-hovercolor","teal-text");
}

var set_link_and_link_title_values = function(options){
	$([link_title,link]).each(function(index,value){
		if(has_new_value(options,value)){
			$("#" + value).val(get_new_value(options,value));
		}
	});
}

var has_initial_value = function(options,link_or_title){
	var has =  ( get_initial_value(options,link_or_title).length == 0 ) ? false : true
	return has;
}

var get_initial_value = function(options,link_or_title){
	var initial_value =  $("#" + options.opener_id).attr("data-" + link_or_title + "-init-value");
	return initial_value;
}

var has_new_value = function(options,link_or_title){
	
	var has =  get_new_value(options,link_or_title).length == 0  ? false : true;
	return has;
}

var get_new_value = function(options,link_or_title){
	var new_value =  $("#" + options.opener_id).attr("data-" + link_or_title);
	return new_value;
}



/***
watch for the link or link-title field becoming empty.
if it becomes empty, and if there is an initial value, 

make its class neither invalid nor valid, so that it reverts back 
to focus class(which is a dark green.)

also hide any error messages.

***/
$(document).on("keydown",link_id,function(e){
	if($(link_id).val().trim().length == 0 ? true : false){
		$(link_id).removeClass("invalid valid");
		$(link_id + "-error-message").hide();
	}
});

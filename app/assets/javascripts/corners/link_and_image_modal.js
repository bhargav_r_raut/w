/****
this file handles the behaviour of the add_a_link and add_an_image
modals.
*****/
var apply_modals = function(id,type){
	if(type==="image"){
		$("#" + id).leanModal({
			before_open: function(options){
				//set the value of the image_url equal to the 
				//return sign_in_required();
				if(sign_in_required(null)){
					return false;
				}


				//so instead of using this image_url_submit_id, better to keep a data-opener-id on the modal itself.
				//$(image_url_submit_id).attr("data-opener-id",options.opener_id);
		    	//$(upload_image_from_pc_id).attr("data-opener-id",options.opener_id);
		    	$(upload_an_image_modal_id).attr("data-opener-id",options.opener_id);
		    	$(image_url_id).val(get_element_image_url());
		    	//console.log("options opener id is:" + options.opener_id);
		    	//console.log("before checking the upload an image modal ");
		    	//console.log($(upload_an_image_modal_id));
		    	/***
				here we have to check if it already has an image, then 
				we render the edit image partial.
				***/
				
				if(has_displayed_image()){
					console.log("it has the displayed image");
					var data_hash = {"units":[],"corner":null};
					//first get the opener id.
					//then get the elements that have that opener_id as element id
					//then filter them so that we get the one which has the attribute data-is-a-corner
					//then we know what to send in the data.
					var corner_or_unit = get_corner_or_unit_from_opener_id(get_opener_id(options.opener_id));
					
					var image = $("img[data-element-id='" + get_opener_id(options.opener_id) + "']");


					//its a corner.
					if(corner_or_unit == 0){
						data_hash["corner"] = {
								"_id" : get_opener_id(options.opener_id),
								"image_id" : image.attr("data-image-id")
							}
						
					}
					else if(corner_or_unit == 1){
						data_hash["units"].push({
									"_id" : get_opener_id(options.opener_id),
									"image_id" : image.attr("data-image-id")
								})
					}

					ajax_call("/elements/edit_image",data_hash,"POST");
				}				
				//it has to set the content of the modal to the basic modal content.
				$("#edit_image_modal_content").remove();
				$("#login_content_upload_an_image").show();
				return true;
			},
			before_close: function(options){
				validate_and_set_image_url();
				return true;
			},
		    after_close: function(options){
		    	//remove those values.
		    	//$(image_url_submit_id).attr("data-opener-id","");
		    	//$(upload_image_from_pc_id).attr("data-opener-id","");
		    	console.log("came to after close");
		    	console.log(upload_an_image_modal_id);
		    	$(upload_an_image_modal_id).attr("data-opener-id","");
		    	console.log("after setting after close");
		    	console.log($(upload_an_image_modal_id));
		    	//clear the input values from the modal.
		    	$(image_url_id).val("");
		    	$(image_url_submit_id).hide();
		    	//need to check about activation and deactivation of the icon.
		    }
	    });
	}
	else if(type===link){

	    $("#" + id).leanModal({

	    	before_open : function(options){
	    		//check if there is an initial value for either the link or the link-title and set it on the field if yes.
	    		if(sign_in_required(null)){
	    			return false;
	    		}
	    		set_link_and_link_title_values(options);
	    		return true;
	    	},
	    	after_open : function(options){
	    		
	    	},
	    	before_close : function(options){
	    		
		    	var empty = $(link_id).val().trim().length == 0 ? true : false;
		    
		    	var valid = validate_link($(link_id).val())

		    	
		    	/***
		    	1)if empty, set the values back to whatever they were initially(stored in the data-link-/link-title-init-value) fields.
				2)make the link icon color to gray again.
		    	***/
		    	if(empty){
		    		revert_link_and_title_values(options);
		    		deactivate_link_icon(options);	
					return true;		    		
		    	}
		    	if(valid){
		    		
		    		$("#link-error-message").hide();
					$(link_id).removeClass("invalid");
					$(link_id).addClass("valid");
					store_link_and_title_values(options);
					activate_link_icon(options);
					clear_fields();
					set_link_icon_tooltip(options);	
		    		return true;
		    	} 
		    	else{
		    		$("#link-error-message").show();
					$(link_id).addClass("invalid");
					$(link_id).removeClass("valid");
					deactivate_link_icon(options);	
					revert_link_and_title_values(options);
					return false;
		    	}
	    	},
	    	dismissible: true, // Modal can be dismissed by clicking outside of the modal
		    opacity: .5, // Opacity of modal background
		    in_duration: 300, // Transition in duration
		    out_duration: 200, // Transition out duration 
	    });
	}
}


var apply_modals_to_el = function(el_id){
	
	$('.editable_images' + '[data-element-id="'+ el_id +'"]').each(function(index){
		apply_modals($(this).attr("id"),"image");
	});

	$('.editable_links' + '[data-element-id="' + el_id + '"]').each(function(index){
		apply_modals($(this).attr("id"),"link");
	});

	initialize_tooltips();
}

/***
@used_in : self.
@params[String] opener_id : the id of the opener element.
@return[Integer] : returns 0 if the opener element is a corner, otherwise returns 1 if it is a unit, and by default returns null if nothing is found.
***/
var get_corner_or_unit_from_opener_id = function(opener_id){
	var corner_or_unit = null;
	$("[data-element-id='" + opener_id + "']").each(function(index){
		if($(this).attr("data-is-a-corner")){
			corner_or_unit = $(this).attr("data-is-a-corner");
		}
	});
	return corner_or_unit;
}
/*****
if the user is not signed in, clicking on any of the contenteditables, add_new_section,publish or any of the icon elements, or the content_holders,
should show the sign_in modal.
*****/
var initialize_tooltips = function(){
	$('.icon-tooltip').tooltipster({
	    delay : 0,
	    theme : 'icon-tooltip-template',
	    hideOnClick : false
	});

	$('.add-section-tooltip').tooltipster({
	    delay : 0,
	    theme : 'add-section-tooltip-template',
	    hideOnClick : false,
	    position: 'top'
	});

	$('.sun-tooltip').tooltipster({
	    delay : 0,
	    theme : 'sun-tooltip-template',
	    hideOnClick : false,
	    position: 'left'
	});

	$('.rain-tooltip').tooltipster({
	    delay : 0,
	    theme : 'rain-tooltip-template',
	    hideOnClick : false,
	    position: 'left'
	});

	$('.storm-tooltip').tooltipster({
	    delay : 0,
	    theme : 'storm-tooltip-template',
	    hideOnClick : false,
	    position: 'left'
	});

	$('.approve-tooltip').tooltipster({
		delay : 0,
		theme : 'icon-tooltip-template',
		hideOnClick: false,
		position: 'left',
		updateAnimation:false
	});

	$('.disapprove-tooltip').tooltipster({
		delay : 0,
		theme : 'icon-tooltip-template',
		hideOnClick: false,
		position: 'right',
		updateAnimation:false
	});	
}
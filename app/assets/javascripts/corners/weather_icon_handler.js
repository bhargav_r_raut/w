function toggle_weather_holder_if_icon_selected(event){
	//if the weather icon holder is open and an element is selected, then collapse the box, to show only the selected element.
	$(".weather_holder").each(function(){
			
	    if (event.target == $(this)[0] || $.contains($(this)[0], event.target)) {
	       
	    } else {
	        console.log("collapse is triggering");
			if($(this).find(":visible").length === 8 && $(this).find(".set").length === 1){
				$(this).find(".set").each(function(){
					$(this).trigger("click");
				});

			}
	    }
	});
}


$(document).on({
    mouseenter: function () {

       	
    },
    mouseleave: function () {
        
      	
        
    },
    mousedown: function(e){
    	
    	if(sign_in_required(e)){
    		return;
    	}

    	var weather_holder = $("#" + $(this).attr("data-element-id") + "_weather_holder");
    	
    	

    	if(weather_holder.hasClass("editable")){
			//make the icon colored.
			$(this).removeClass("dull");

			//add the set class on it for other handlers.
			$(this).addClass("set");
			
			//make all the children of the parents siblings dull.
			$(this).parent().siblings().not(".icon_warning").children().addClass("dull");

			//unset all the above..
			$(this).parent().siblings().not(".icon_warning").children().removeClass("set");
			
			var weather_holder = $("#" + $(this).attr("data-element-id") + "_weather_holder");

			//say that the icon is set.
			weather_holder.attr("data-icon-set",true);

			//state the id of the selected icon.
			weather_holder.attr("data-icon-set-id",$(this).attr("data-weather-id"));

			weather_holder.addClass("dirty");

			//if the icon warning is seen, then get rid of it.
			$(this).parent().siblings().each(function()
			{
				if($(this).hasClass("icon_warning"))
				{
					$(this).hide();
				}
			});

			var submit_corner_called = false;

			$(this).parent().siblings().not(".icon_warning").toggle("fast",function(){	
				
				if(submit_corner_called == false && weather_holder_collapsed(weather_holder)){
					console.log("calling submit corner from weather icon");
					submit_corner(e);
					submit_corner_called = true;
				}
				
			});

			submit_corner_called = false;


		}

    }
}, ".weather");


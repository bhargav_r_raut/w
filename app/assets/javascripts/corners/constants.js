/***
constants
***/
var link = "link";
var link_title = "link-title";
var link_id = "#" + link;
var link_title_id = "#" + link_title;
var image_url = "image_url";
var image_url_id = "#" + image_url;
var image_url_submit = "image_url_submit";
var image_url_submit_id  = "#" + image_url_submit;
var upload_image_from_pc = "upload_image_from_pc";
var upload_image_from_pc_id = "#" + upload_image_from_pc;
var upload_an_image_modal = "upload_an_image_modal";
var upload_an_image_modal_id = "#" + upload_an_image_modal;
var SPAM = "spam";
var IMAGE = "image";
var CONTENT = "content";
var TITLE = "title";

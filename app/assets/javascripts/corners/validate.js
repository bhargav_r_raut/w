var validate_link = function(link){
    return /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i.test( link);
}



//on focus a unit, check weather_holders of all previous siblings units,if they dont have a set value, show the warning.
//
var show_icon_warning = function(data_element_id){
    $("#" + data_element_id + "_icon_warning").show();
    return false;
}

//on focus a unit, check prev unit titles, if they have icons, then show the warning.
var show_unit_title_or_intro_content_warning = function(data_element_id){
    $("#" + data_element_id + "_title_warning").show();
    return false;
}

//only show when 
var show_corner_title_warning = function(data_element_id){
    //remove the mdi-content-create and replace with warning class.
    $('i[data-edit-corner-icon="true"]').removeClass("mdi-content-create").addClass("mdi-alert-error");
    $('i[data-edit-corner-icon="true"]').tooltipster("content","You need to add a title to your corner");
    return false;
}

/***
arguments:
e -> click_event 
whatever was the click event that caused the validation to fire.
****/
var validate_all_units = function(e){
    var data_corner_id = $(e.target).attr("data-corner-id");
	var valid = true;
	$('.unit[data-corner-id="'+ data_corner_id +'"]').each(function(index){
            var unit_valid = validate_unit($(this).attr("data-element-id"),e);
			if(unit_valid === false){
				valid = unit_valid;
			}
	});
	return valid;
}

var validate_corner_title = function(params,event){
    
    var data_corner_id = $(event.target).attr("data-corner-id");
    var element_id = $(event.target).attr("data-element-id");
    if(attribute_previously_committed(element_id,"title")){
        console.log("attribute title has been previously committed");
    }
    else if((params["corner"] === null || !(params["corner"]["title"])) && !corner_title_edited(data_corner_id)){
        show_corner_title_warning();
        return false;
    }   
    else if(params["corner"] && params["corner"]["title"] && params["corner"]["title"].trim() == ""){
        return false;
    }
    return true;
}

/****
arguments:
unit_element_id -> the element id of the unit
_self -> a reference to the jquery object for the unit.
event -> the click event that has triggered the validation.



****/
var validate_unit = function(unit_element_id,event){
    
	var unit_ceditable = $("#" + unit_element_id + "_title");
	var unit_wholder = $("#" + unit_element_id + "_weather_holder");
    var data_icon_set = unit_wholder.attr("data-icon-set");
    var title_text = unit_ceditable.text().trim();
    var link = $("#" + unit_element_id + "_element").find(".ediable_links").first();
    var link_title = link.attr("data-link-title");
    var link_url = link.attr("data-link");
    
    /****
	if the event target is a weather icon, and its data element id is the same as the unit id provided, then it means that the weather icon was clicked.
    ****/
    
    var unit_weather_icon_clicked = ($("#" + event.target.id).attr("data-element-id") == unit_element_id) && ($("#" + event.target.id).hasClass("weather")) ? true : false;

    /***
    if the event target is a ceditable, and its data element id is the same as the unit id provided, then it means taht the ceditable was clicked.
    ****/
    var unit_ceditable_clicked = ($("#" + event.target.id).attr("data-element-id") == unit_element_id) && ($("#" + event.target.id).hasClass("utitle")) ? true : false;
        
    

    var icon_warning;
   
    if(data_icon_set != "false"){
        if(title_text == ""){
            
            icon_warning = show_unit_title_or_intro_content_warning(unit_element_id);
        }
        else{
            
            icon_warning = true;
        }
    }
    else{
        
        icon_warning = show_icon_warning(unit_element_id);
    }

   
    return icon_warning;


}



$(document).on('click','.icon_warning',function(event){
    $(this).tooltipster("show");
});


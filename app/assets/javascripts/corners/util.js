var get_unit_id_from_string_id = function(string_id){
	return string_id.replace(/\D/g, '');
}


var is_empty = function(string){
    return string.trim().length == 0;
}

var is_a_corner = function(el){
	return el.attr("data-element-id") == el.attr("data-corner-id");
}


/***
given an element will determine it belongs to a corner or a unit.
returns null if the element belongs to neither.
***/
var belongs_to_corner = function(el){
	

	var data_element_id;
	if(el.hasAttribute('data-element-id')){
		console.log("it has data-element-id:");
		data_element_id = el.attr('data-element-id');
	}
	else if(el.hasAttribute('data-opener-id')){
	   console.log("it has data-opener-id");
	   data_element_id = $("#" + el.attr('data-opener-id')).attr("data-element-id");
	}
	else{
		console.log("it has neither");
		return null;
	}
	console.log("data-element-id is determined as:" + data_element_id);
	console.log("looking for the type element:");
	console.log($("#" + data_element_id + "_type"));
	console.log("//////////////////////////////-----------ends----------");
	return $("#" + data_element_id + "_type").attr("data-is-a-corner") == 0;
}


var get_corner_id_and_persisted_corner_id = function(){
	//alert("change detected");
	var corner_element = $("div").find("[data-is-a-corner='0']").first();
	
	var corner_id = corner_element.attr("data-corner-id");

	var persisted_corner_id = corner_element.attr("data-persisted-corner-id");

	return {"corner_id":corner_id, "persisted_corner_id":persisted_corner_id}
}

var get_version = function(el,corner_or_unit){

	var data_element_id = el.attr("data-element-id");
	return $("#" + data_element_id + "_" + corner_or_unit + "_version").val();
}

/***
used in corner_created.js.erb
****/
var get_element_by_data_element_id = function(id,corner_or_unit){
	return $('.' + corner_or_unit + '[data-element-id="'+ id +'"]');
}


var get_corner_about_holder = function(){
	return $("#corner_about_holder");
}

/***
@param[String] cls : class name without preceeding dot.
@param[String] data-el-id : the id of the element(corner/unit) as a string
@return[Array] : returns the jquery element set or nil if none is found.
***/
var get_el_by_class_and_data_el_id = function(cls,data_el_id){
	return $('.' + cls + '[data-element-id="'+ data_el_id +'"]');
}

/***
returns the title-element of the corner, given the data-element-id
currenty used in validate.js to check if the corner has an 
***/
var get_corner_title = function(data_element_id){
     var corner_title = $('.ctitle[data-element-id="'+ data_element_id +'"]').first();
     return corner_title;
}

var corner_title_edited = function(data_element_id){
	return get_corner_title(data_element_id).hasClass("edited");
}

/***
@param[String] element_id : the id of the corner or the unit element
@param[String] attribute : title/content/image/link

if the attribute is not new, i.e it was committed in a previous commit, then it will have an attribute id. 

@return[Boolean] : true if the attribute id exists, false otherwise.
***/
var attribute_previously_committed = function(element_id, attribute){
	if(get_attribute_id(element_id,attribute)){
		return true;
	}
	return false;
}

/***
@param[String] element_id : the id of the corner or the unit element
@param[String] attribute : title/content/image/link

The root node / main wrapper node of the corner or unit, has all the attribute ids dumped in it. eg: data-title-id, data-content-id, data-link-id, data-image-id. First get a reference to the root node,
then get the relevant attribute id.

@return[String] attribute_id : returns the attribute id or null if it has not been set yet.
***/
var get_attribute_id = function(element_id,attribute){
	var element_root_node = $("#" + element_id + "_type");
	return element_root_node.attr("data-" + attribute + "-id");
}

var get_previous_unit = function(current_unit_data_element_id){


	//console.log("current element data id is:" + current_unit_data_element_id);
	var prev_element = $("#" + current_unit_data_element_id + "_type").parent().prev();
    if(prev_element.length){
    	//console.log("previous elememt is :" + prev_element);
    	var prev_element_id = prev_element.attr("data-element-id");    
    	if(prev_element_id){
    		//console.log("prev element id is:" + prev_element_id);
    		var prev_element_type_holder = $("#" + prev_element_id + "_type");
    		if(prev_element_type_holder){
    			//console.log("prev element type holder is:" + prev_element_type_holder);
		    	var prev_element_is_a_corner = prev_element_type_holder.attr("data-is-a-corner");	
		    	if(prev_element_is_a_corner && prev_element_is_a_corner == 1){
		    		//console.log("prev element is a unit");
		    		var prev_element = $("#" + prev_element_id + "_element");
		    		var prev_contenteditable = $("#" + prev_element_id + "_title");
		    		var prev_weather_holder = $("#" + prev_element_id + "_weather_holder");
		   			return {"p_ceditable": prev_contenteditable, "p_wholder" : prev_weather_holder, "p_element":prev_element};
		   		}
		   		else{
		   			//console.log("prev element is not a unit it is:" + prev_element_is_a_corner);
		   		}
    		
    		}
    		else{
    			//console.log("prev element type holder was not found.");
    		}
    	}	
    	else{
    		//console.log("prev element id was not found.");
    	}
    }
    else{
    	//console.log("previous element is undefined");
    }

    return {};

}

/***
checks to see if the weather holder of the same unit as the contenteditable which was focusedout has been clicked or not.
returns: true/false
***/
var sibling_weather_holder_clicked = function(ceditable_el_id){
	
	//console.log("clicky is:");
	//console.log(clicky);
	if(clicky != null){
		//console.log("does click have class weather");
		//console.log(clicky.hasClass("weather"));
		//console.log("is clicky's dataattribute not nil");
		//console.log(clicky.attr("data-element-id") != null);
		//console.log("is the data element id equal to the ceditable el id");
		//console.log(clicky.attr("data-element-id") == ceditable_el_id);

		if(clicky.hasClass("weather")){
			//console.log("has weather");
			if(clicky.attr("data-element-id") != null){
				//console.log("data el id is not null");
				if(clicky.attr("data-element-id") == ceditable_el_id){
					//console.log("el ids are equal");
					return true;
				}
				else{
					return false;
				}
			}
			else{
				return false;
			}
		}	
		else{
			return false;
		}
	}
	else{
		return false;
	}
}

/***
given a weather holder element, it checks if it is collapsed or 
open.
***/
var weather_holder_collapsed = function(weather_holder){
	var collapsed_count = 0;
	weather_holder.find(".weather").each(function(index){
		if($(this).is(":visible")){

		}	
		else{
			collapsed_count++;
		}
	});

	//if it is not collapsed, then we return false.
	return (collapsed_count == 2);
}

/***

***/
$(document).on('focusout','#about',function(event){

	//if there is a current user div and there is some text in the about, then show 
	//that div.
	if($(this).text().trim()!=""){

		$(".new_corner_time").show();
	}
	else{
		$(".new_corner_time").hide();
	}

});


$(document).on('click','#add-section',function(event){
	if(!sign_in_required(event)){
		$("#add_new_section_form").submit();
	}	
});



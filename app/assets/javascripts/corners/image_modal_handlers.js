/***********************************************************************
FUNCTIONS FOR UPLOAD_AN_IMAGE_MODAL
************************************************************************/


//TODO:

/*****
on file upload, the activate_link_icon function has to be called.
this will be called in the js erb that is returned on success .
*****/

/***
when the upload an image from a link is clicked, then the arrow for submitting that image is shown.
***/
$(document).on({
	click: function(){
		$(image_url_submit_id).show();
	}

},image_url_id);

/***
if the image url is empty, then the warning is hidden.
***/
$(document).on('keyup',image_url_id,function(event){
	if($(this).val().trim().length == 0){
		$("#image-url-error-message").hide();
		//make the class of the input valid.
		$(this).removeClass("invalid valid");

	}
});
//handling the submission of a url instead of an image.
$(document).on('click',image_url_submit_id,function(event){
	validate_and_set_image_url();
});


//handling image file chosen from pc.
$(document).on("click",upload_image_from_pc_id,function(event){
	
	var opener_element = get_opener_element($(upload_an_image_modal_id));

	var data_element_id = get_element_id(opener_element);
	var data_corner_id = get_corner_id(opener_element);
	var corner_or_unit = (data_corner_id == data_element_id) ? "corner" : "unit";
	//var corner_or_unit = (belongs_to_corner($(this)) ? "corner" : "unit");
	//if data-corner-id and data-element-id is same then it is a corner, otherwise it is a unit

	
	var trigger_target_id = "#" + data_element_id + "_" + corner_or_unit + "_image";
	
	$(trigger_target_id).trigger("click");
});



//submit the form if either the upload_image or the upload_image_url changes.
$(document).on('change','.upload_image, .upload_image_url',function(event){
	var form = $(this).closest("form");
	form.submit();
});



/****
basic function is called when submit_image_url is clicked
also called in before_close callback of the modal.
*****/
var validate_and_set_image_url = function(){
	var image_url = $(image_url_id).val();
	var is_url_valid = validate_link(image_url);

	if((!(image_url in window)) || !(image_url_id in window) || is_empty($(image_url_id).val())){
		unset_element_image_url();
		deactivate_image_icon();
	}
	else if(is_url_valid){
		$(image_url_id).removeClass("invalid");
		$(image_url_id).addClass("valid"); 
		$("#image-url-error-message").hide();
		set_element_image_url(image_url);
		activate_image_icon();
	}
	else{
		//show the error message.
		$("#image-url-error-message").show();
		$(image_url_id).removeClass("valid");
		$(image_url_id).addClass("invalid");
		unset_element_image_url();
		deactivate_image_icon();
	}

}




/***
gets the opener element, of the modal, if we click on any element inside the image modal. This currently only applies the image-url-submit button, and the upload_image_from_pc icon.
***/
var get_opener_element = function(element){
	return $("#" + element.attr("data-opener-id"));
}

/***
gets the data-element-id of the given element.
expects a jquery element to be passed in.
****/
var get_element_id = function(el){
	return el.attr("data-element-id");
}

var get_corner_id = function(el){
	return el.attr("data-corner-id");
}

/****
when image_url_submit is clicked, and the url is valid, this function sets the value on the form element.

after setting the value, we trigger a change , so that the form submits.

****/
var set_element_image_url = function(image_url){

	var image_url_form_element = get_image_url_form_element();

	//set the value of the image url.
	image_url_form_element.val(image_url);

	//trigger the change in the value, so that the form submits.
	image_url_form_element.trigger("change");

}

/****
makes the value of the image url element nothing.
****/
var unset_element_image_url = function(){
	
	var image_url_form_element = get_image_url_form_element();

	//set the value of the image url.
	image_url_form_element.val("");

}


/***
returns the value of the image url if any already exists. on the form element.
****/
var get_element_image_url = function(){
	console.log("came to get element image url");
	var image_url_form_element = get_image_url_form_element();

	return image_url_form_element.val();

}

/***
There is an element in the hidden form , before each (corner or unit), that has an element which has the following id format:

element_id + _ + corner_or_unit + _image_url

this function returns that element.

it is used in the getters and setters above.
***/
var get_image_url_form_element = function(){
	
	var opener_element_id = get_element_id(get_opener_element($(upload_an_image_modal_id)));

	var image_url_form_element = $("#" + opener_element_id + "_"  + (belongs_to_corner($(image_url_submit_id)) ? "corner" : "unit") + "_image_url");

	return image_url_form_element;
}

/****
activates the image icon in the options bar
will make it dark-blue if the link is valid otherwise leaves it grey.
****/
var activate_image_icon = function(){
	
	var image_icon_el = get_image_icon_element();
	image_icon_el.removeClass("grey-text teal-text text-lighten-2").addClass("blue-grey-text text-darken-2");
}

/****
deactivate the image icon.
****/
var deactivate_image_icon = function(){
	console.log("came to deactivate image icon");
	//if there is no image being displayed
	if(!has_displayed_image()){
		var image_icon_el = get_image_icon_element();
		//provided that this is not the intro icon, because there we wanna have different behaviour.
		if(belongs_to_corner(image_icon_el)){
			console.log("this belongs to a corner");
		}
		else{
			image_icon_el.removeClass("blue-grey-text text-darken-2").addClass("grey-text text-lighten-2");	
		}
	}

}

/****
gets the image icon element.(the small camera icon or the side camera icon.)
****/
var get_image_icon_element = function(){
	return get_opener_element($(upload_an_image_modal_id));
}

var get_opener_id = function(id_string){
	return id_string.substring(0,id_string.indexOf("_"));
}

var get_opener_id_from_opener_element = function(opener_element){
	return get_opener_id(opener_element.attr("id"));
}

/***
a partial called image_partial is rendered if there is an image on this unit/corner. in that case, there will exist a element called (el_id)_image, so we check for that element. if its present, we return true.
this function is used in the deactivate_image_icon function.
if there is an image, then, we don't deactivate, even if deactivate is called because there is nothing in the image_url box.
****/
var has_displayed_image = function(){
	
	var opener_id = get_opener_id_from_opener_element(get_opener_element($(upload_an_image_modal_id)));
	return $("#" + opener_id + "_image").exists();
}

/***
@used_in: views/corners/_corner_created.js.erb
@used_to: close the image modal post image_upload or image_crop.
***/
var close_image_modal = function(){
	if($("#upload_an_image_modal").is(':visible')){
		$("#upload_an_image_modal").closeModal();
	}
}

/****
on click delete image, should call submit_corner.
****/
$(document).on('click','.delete_image',function(event){
	$(this).toggleClass("dirty");
	submit_corner(event);
});

/***
handle the image crop, form submission.
****/
//on clicking crop, we can call submit_corner(event)
//and we should modify build_form to pick up relevant data.
//modify the papercrop helper "cropbox" to accept the data-corner-id
//also it should assign a class, so that it can all be made dirty.
$(document).on('click','#crop_button',function(event){
	//get all the crop attributes.
	console.log("clicked crop.");
	$(".crop-attribute").each(function(index){
		$(this).addClass("dirty");
	});
	submit_corner(event);
});

/***
on clicking any image, it should trigger a click on the nearest camera icon.

***/
$(document).on('click','.image',function(event){
	//find the nearest camera.
	var el_id = $(this).attr("data-element-id");
	if(is_a_corner($(this))){
		$("#" + el_id + "_intro_icon").trigger("click");
	}	
	else{
		$("#" + el_id + "_add_image").trigger("click");
	}
});
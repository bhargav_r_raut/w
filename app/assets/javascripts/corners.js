//= require corners/constants.js
//= require corners/util.js
//= require corners/validate.js


//= require corners/options_toggle.js
//= require corners/submit_corner_or_unit.js
//= require corners/tooltip_handler.js
//= require corners/weather_icon_handler.js
//= require corners/image_modal_handlers.js
//= require corners/link_and_image_modal.js
//= require corners/link_modal_handlers.js
//= require corners/floating_update_button.js
//= require element_height_adjustment.js
//= require corners/spam.js



var corners_on_load = function(){
	console.log("called corners on load");
	if(should_js_run("corner") === false){
		//console.log("returned from corners");
		return;
	}
	

	 //on init apply the modals to all the editable images and editale links classes elements
	 $(".editable_images").each(function(index){
	 	
	 	apply_modals($(this).attr("id"),"image");

	 });

	 $(".editable_links").each(function(index){
	 	
	 	apply_modals($(this).attr("id"),"link");

	 });



	 initialize_tooltips();
		
};





$(document).on('turbolinks:load',corners_on_load);


$(document).on('click','body',function(event){

	//toggle_weather_holder_if_icon_selected(event)
	//here there is the exception that if a unit has 
	///validate_all_units(event);

});





/**

//load the views chart when the stats are clicked.
$(document).on('click','#stats_toggle',function(){

	$('#stats').slideToggle("fast",function(){
		//if the stats is visible,then load the chart.
		//if not dont do anything.
		if($("#stats").css("display") === "block"){
			console.log("got the stats as displayed");
			var ctx = $("#views_chart").get(0).getContext("2d");
		

			var data = {
		    labels: ["4 am", "5 am", "6 am", "7 am", "12pm", "3 pm", "7 pm","9pm","10pm","12pm"],
			    datasets: [
			        {
			            label: "My First dataset",
			            fillColor: "rgba(220,220,220,0.2)",
			            strokeColor: "rgba(220,220,220,1)",
			            pointColor: "rgba(220,220,220,1)",
			            pointStrokeColor: "#fff",
			            pointHighlightFill: "#fff",
			            pointHighlightStroke: "rgba(220,220,220,1)",
			            data: [65, 59, 80, 81, 56, 55, 40,100,200,120],
			            tooltip_objects:[]
			        }
			    ]
			};
			

			var myLineChart = new Chart(ctx).Line(data, {});
				}
			});

});




$(document).on({
    mouseenter: function () {
    	$(this).removeClass("grey-text text-lighten-2");
   		$(this).addClass("teal-text text-darken-2");
    },
    mouseleave: function () {
        
      	$(this).removeClass("teal-text text-darken-2");
   		$(this).addClass("grey-text text-lighten-2");
        
    },
    click: function(){
    	//make ajax request and then remove the element completely.
    }
},".any_moderation");


**/
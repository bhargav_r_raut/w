module ElementControllerHelper

	def invalid_corner
		invalid_element
	end

	def invalid_unit	

	end

	def invalid_element
		el = empty_element
		el["title"] = ""
		return el
	end

	def empty_element
		el = Element.new
		el["_id"] = el["_id"].to_s
		return el.attributes
	end

	def empty_unit(corner_attributes)
		u = empty_element
		u.delete("_id")
		u = Unit.new(u).attributes
		u["_id"] = u["_id"].to_s
		u["corner_id"] = corner_attributes["_id"]
		return u
	end

	def empty_unit_with_image(corner_attributes)
		u = empty_element
		u.delete("_id")
		u = Unit.new(u)
		u.image = File.new("app/assets/images/facebook.png")
		u = u.attributes
		u["_id"] = u["_id"].to_s
		u["corner_id"] = corner_attributes["_id"]
		return u
	end

	def unit_with_image_and_title(corner_attributes)
		u = empty_unit_with_image(corner_attributes)
		u["title"] = "hello"
		return u
	end


	def empty_corner_with_image
		c = empty_element
		c.delete("_id")
		c = Corner.new(c)
		
		#c.image = Rack::Test::UploadedFile.new( )
		#cover_image = File.new(Rails.root.join("app/assets/images/facebook.png"))
		#c.image = ActionDispatch::Http::UploadedFile.new(tempfile: cover_image, filename: File.basename(cover_image), type: "image/png")
		c.image = File.new(Rails.root.join("app/assets/images/facebook.png"))
		c = c.attributes
		c["_id"] = c["_id"].to_s
		return c
	end

	def empty_corner
		c = empty_element
		c.delete("_id")
		c = Corner.new(c).attributes
		c["_id"] = c["_id"].to_s
		return c
	end

	def valid_element
		el = Element.new(:title => Faker::Lorem.sentence, :content => Faker::Lorem.sentence)
		el["_id"] = el["_id"].to_s
		return el.attributes
	end

	def valid_corner
		v = valid_element
		v.delete("_id")
		c = Corner.new(v).attributes
		c["_id"] = c["_id"].to_s
		return c
	end

	def valid_unit(corner_attributes=nil)
		v = valid_element
		v.delete("_id")
		u = Unit.new(v).attributes
		u["_id"] = u["_id"].to_s
		if corner_attributes.nil?
			corner_attributes = Corner.new.attributes
			corner_attributes["_id"] = corner_attributes["_id"].to_s
		end
		u["corner_id"] = corner_attributes["_id"]
		u["icon"] = 1
		return u
	end

	
	
	#@param attributes(Array) : array of attributes which are to be simulated as being invalid. defaults to all attributes being invalid.
	def element_with_invalid_attributes(attributes=[])
		if attributes.empty?
			attributes = ["title","content","link","link_title","image_url"]
		end
		el = Element.new
		attributes.each do |att|
			if ["title","content","link_title"].include? att
				##these are all textual , so we make it a large empty space, this should get stripped and then fail on the min length validation.
				el[att] = "     "
			else
				##these are the image_url and link,basically both urls.
				el[att] = "dog"
 			end
		end
		el = el.attributes
		el["_id"] = el["_id"].to_s
		return el

	end
	##use this method to check whether whatever was sent in the params is there in the response.
	##it also ensures that errors and messages are empty.
	def params_should_equal_response(response,params,curr_user,expect_errors = false)

		corner = response.corner
		units = response.units
		errors = response.errors
		messages = response.messages

		if !expect_errors
			errors.should be_empty
			messages.should be_empty
		end
		
		##conver the units to a hash keyed by the unit id.

		
		u_hash = Hash[units.map{|u|
			u = [u.id.to_s,u]
		}]

		if !params[:corner].nil?
			el_should_eq_param_el(corner,params[:corner],curr_user)
		end	

		if !params[:units].nil? && !units.empty?

			params[:units].each do |unit|

				u_hash.keys.should include(unit["_id"])
				el_should_eq_param_el(u_hash[unit["_id"]],unit,curr_user)

			end

		end	



	end

	def el_should_eq_param_el(el,param_el,curr_user)

		el.should_not be_nil
		el.contributor_ids.last[0].should eq(curr_user.id.to_s)
		el.id.to_s.should eq(param_el["_id"])


		if !param_el["title"].nil?
			el.title.should eq(param_el["title"])
			##check that the curr_user is a contributor for title
			curr_user.is_contributor?(el,"title").should be
		end

		if !param_el["content"].nil?
			el.content.should eq(param_el["content"])
			##check that the curr_user is a contributor for content.
			curr_user.is_contributor?(el,"content").should be
		end

		if !param_el["link"].nil?
			el.link.should eq(param_el["link"])
			curr_user.is_contributor?(el,"link").should be
		end

		

	end

end
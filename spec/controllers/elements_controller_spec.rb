require 'rails_helper'

=begin
describe ElementsController do

	login_user
	
	before(:each) do 
		Element.delete_all
	end


	context " --- creation --- " do 


		context " --- corner --- " do 

			it " --- creates --- " do 
				
				corner = valid_corner
				@params = {:corner => corner, :format => 'js'}	
				post :create, @params 
				response = assigns(:r)
				params_should_equal_response(response,@params,subject.current_user)
				Corner.count.should eq(1)
				
			end

			it " --- empty corner with image --- " do 

				corner = empty_corner_with_image
				@params = {:corner => corner, :format => 'js'}
				post :create, @params
				response = assigns(:r)
				response.errors.should be_empty
				Corner.count.should eq(1)
				response.corner.image.should_not be_nil

			end


		end

		context " --- unit --- " do 

			context " --- single --- " do 

				context " --- corner provided in params --- " do 

					it " --- should create if corner id and unit id is same ---" do 

						corner = valid_corner
						unit = valid_unit(corner)

						#puts JSON.pretty_generate(unit)

						@params = {:units => [unit], :corner => corner, :format => 'js'}	

						post :create, @params 

						response = assigns(:r)

						params_should_equal_response(response,@params,subject.current_user)
		
						Unit.count.should eq(1)

					end

					it " --- unit not created if its corner id is different from provided corner id ---" do 

						corner = valid_corner
						##make the unit from another valid corner.
						unit = valid_unit(valid_corner)	

						@params = {:units => [unit], :corner => corner, :format => 'js'}

						post :create, @params 

						response = assigns(:r)

						params_should_equal_response(response,@params,subject.current_user,true)
			
						response.errors.should_not be_empty
						Unit.count.should eq(0)

					end

				end

				context " --- corner not in params --- " do 

					it " --- should create unit if its corner id exists in database ---" do 
					
						corner = valid_corner
						@params = {:corner => corner, :format => 'js'}	
						post :create, @params

						response = assigns(:r)

						params_should_equal_response(response,@params,subject.current_user)
						Corner.count.should eq(1)

						##################now try to create a unit with this corner id.

						unit = valid_unit(corner)
						@params = {:units => [unit],:format => 'js'}
						post :create, @params
						response = assigns(:r)
						params_should_equal_response(response,@params,subject.current_user)
						Unit.count.should eq(1)

					end

					it " --- should not create unit if its corner id does not exist in the database --- " do 

						corner = valid_corner					
						unit = valid_unit(corner)
						@params = {:units => [unit],:format => 'js'}
						post :create, @params
						response = assigns(:r)
						##the true is so that it does not check if the errors 
						#are empty in the params_should_equal_response function. by default this is false, so that it always checks in all the other tests if the 
						params_should_equal_response(response,@params,subject.current_user,true)
						response.errors.should_not be_empty
						Unit.count.should eq(0)

					end

				end	

			end

			context " --- multiple --- " do 

				context " --- corner in params --- " do

					context " all units have a non nil corner id " do 

						it " --- should create all if all their corner id == given corner --- " do
							corner = valid_corner
							unit = valid_unit(corner)
							unit1 = valid_unit(corner)
							unit2 = valid_unit(corner)
							@params = {:units => [unit,unit1,unit2], :corner => corner, :format => 'js'}
							post :create, @params
							response = assigns(:r)
							params_should_equal_response(response,@params,subject.current_user)
							Unit.count.should eq(3)
	
						end

						it " --- should not create any if all their corner id != given corner --- " do 

							corner1 = valid_corner
							corner2 = valid_corner
							unit = valid_unit(corner1)
							unit1 = valid_unit(corner2)
							@params = {:units => [unit,unit1], :corner => corner1, :format => 'js'}
							post :create, @params
							response = assigns(:r)
							params_should_equal_response(response,@params,subject.current_user,true)
							Corner.count.should eq(1)
							Unit.count.should eq(0)
							response.errors["units"].should eq({"corner_ids" => "the corner ids on the units are not the same"})

						end

					end

					context " --- some units have a nil corner id, and rest have the same corner id as the provided corner --- " do

						it " --- should assign the given corner's id to the nil units, and create all units --- " do 

							corner = valid_corner
							
							##two units with nil corner id
							unit = valid_unit(corner)
							unit["corner_id"] = nil
							unit1 = valid_unit(corner)
							unit1["corner_id"] = nil

							##one unit with the corner id of the corner provided in the params
							unit2 = valid_unit(corner)


							@params = {:units => [unit,unit1,unit2], :corner => corner, :format => 'js'}
							post :create, @params
							response = assigns(:r)
							params_should_equal_response(response,@params,subject.current_user)
							Corner.count.should eq(1)
							Unit.count.should eq(3)
							response.units.each do |u|
								u.corner_id.should eq(response.corner.id)
							end


						end

					end

					context " --- some units have a nil corner id, some have same corner id as the provided corner, some have a different corner id --- " do

						it " --- should not create any units --- " do 

							corner = valid_corner
							
							##two units with a nil corner id
							unit = valid_unit(corner)
							unit["corner_id"] = nil
							unit1 = valid_unit(corner)
							unit1["corner_id"] = nil

							##one unit with 
							unit2 = valid_unit(corner)

							##make a third unit with a totally different corner
							unit3 = valid_unit(valid_corner)


							###
							@params = {:units => [unit,unit1,unit2,unit3], :corner => corner, :format => 'js'}
							post :create, @params
							response = assigns(:r)
							params_should_equal_response(response,@params,subject.current_user,true)
							Corner.count.should eq(1)
							Unit.count.should eq(0)
							response.errors["units"].should eq({"corner_ids" => "the corner ids on the units are not the same"})

						end


					end

				end

				context " --- corner not in params --- " do 

					context " --- all have non nil corner id --- " do 

						it " --- should create all if all have same corner id --- " do 
							
							corner = valid_corner
							@params = {:corner => corner, :format => "js"}
							post :create, @params

							unit = valid_unit(corner)
							unit1 = valid_unit(corner)
							unit2 = valid_unit(corner)
							@params = {:units => [unit,unit1,unit2], :format => 'js'}
							post :create, @params
							response = assigns(:r)
							params_should_equal_response(response,@params,subject.current_user)
							Unit.count.should eq(3)

						end

					end

					context "--- some units have nil corner id, others have the same corner id ---" do 

						it " --- will create the non nil units --- " do 

							corner = valid_corner
							@params = {:corner => corner, :format => "js"}
							post :create, @params

							unit = valid_unit(corner)
							unit["corner_id"] = nil

							unit1 = valid_unit(corner)
							unit1["corner_id"] = nil

							unit2 = valid_unit(corner)
							unit3 = valid_unit(corner)


							@params = {:units => [unit,unit1,unit2,unit3], :format => 'js'}
							
							post :create, @params
							
							response = assigns(:r)
							
							params_should_equal_response(response,@params,subject.current_user,true)


							response.errors.should_not be_empty

							
							expected_ids = [unit2["_id"],unit3["_id"]]

							response.units.each do |u|
								if u.errors.empty?
									present = expected_ids.include? u.id.to_s
									present.should eq(true)
								end
							end

							Unit.count.should eq(2)


						end

					end

					context " --- some have nil corner ids, all others have dissimilar corner ids --- " do 

						it " --- will not create any units ---" do 

							corner = valid_corner
							@params = {:corner => corner, :format => "js"}
							post :create, @params

							##build the units with nil corner ids.
							unit = valid_unit(corner)
							unit["corner_id"] = nil

							unit1 = valid_unit(corner)
							unit1["corner_id"] = nil

							##build units with different corner ids.
							unit2 = valid_unit(valid_corner)
							unit3 = valid_unit(valid_corner)
							@params = {:units => [unit,unit1,unit2,unit3], :format => 'js'}
							post :create, @params

							response = assigns(:r)
							
							params_should_equal_response(response,@params,subject.current_user,true)

							response.errors.should_not be_empty

							Unit.count.should eq(0)

						end


					end

				end

			end


		end


		context " --- corner and unit --- " do 


			it " --- creates corner and unit, provided both valid --- " do 

				corner = valid_corner
				unit = valid_unit(corner)
				@params = {:corner => corner, :units => [unit], :format => 'js'}
				post :create, @params 
				response = assigns(:r)
				params_should_equal_response(response,@params,subject.current_user)
				Corner.count.should eq(1)
				Unit.count.should eq(1)


			end

			it " --- creates corner if unit is invalid --- " do 

				c = valid_corner
				u = empty_unit(c)
				
				@params = {:corner => c, :units => [u], :format => 'js'}
				post :create, @params 
				response = assigns(:r)
				Corner.count.should eq(1)
				Unit.count.should eq(0)
				response.errors.should_not be_empty

			end

			context " --- unit with only image --- " do 

				context " --- single unit --- " do 

					context " --- corner in params(empty or full) --- " do 

						it " --- creates unit, if unit has image " do 

							c = empty_corner
							u = empty_unit_with_image(c)
							@params = {:corner => c, :units => [u], :format => 'js'}
							post :create, @params

							Corner.count.should eq(0)
							Unit.count.should eq(1)


						end


						it " --- does not create if unit has any other attrs --- " do 

							c = empty_corner
							u = valid_unit(c)
							@params = {:corner => c, :units => [u], :format => 'js'}
							post :create, @params
							Corner.count.should eq(0)
							Unit.count.should eq(0)

						end


						it " --- does not create if unit has image + other attrs --- " do 

							c = empty_corner
							u = unit_with_image_and_title(c)
							@params = {:corner => c, :units => [u], :format => 'js'}
							post :create, @params
							Corner.count.should eq(0)
							Unit.count.should eq(0)


						end


					end


					context " --- corner not in params --- " do 

						it " --- creates unit, if unit has image " do 

							##for the unit to be created it has to have a 
							## a corner id.
							c = empty_corner
							u = empty_unit_with_image(c)
							@params = {:units => [u], :format => 'js'}
							post :create, @params

							Corner.count.should eq(0)

							Unit.count.should eq(1)


						end


						it " --- does not create if unit has any other attrs --- " do 

							c = empty_corner
							u = valid_unit(c)
							@params = {:units => [u], :format => 'js'}
							post :create, @params
							Corner.count.should eq(0)
							Unit.count.should eq(0)

						end


						it " --- does not create if unit has image + other attrs --- " do 

							c = empty_corner
							u = unit_with_image_and_title(c)
							@params = {:units => [u], :format => 'js'}
							post :create, @params
							Corner.count.should eq(0)
							Unit.count.should eq(0)


						end


					end

				end

			end

			context " --- invalid corner --- " do 

				it " --- does not create units --- " do 

					c = invalid_corner
					u = valid_unit(valid_corner)
					@params = {:corner => c, :units => [u], :format => 'js'}
					post :create, @params
					Corner.count.should eq(0)
					Unit.count.should eq(0)

				end

			end


		end

	
		context " --- validations --- " do 

			it " --- strips empty spaces on textual fields --- " do 
				el = element_with_invalid_attributes
				@params = {:corner => el, :format => "js"}
				post :create, @params
				response = assigns(:r)
				response.corner.title.should eq("")
			end

			it " --- has even one invalid attribute --- " do 
					
				el = element_with_invalid_attributes
				el["_id"] = el["_id"].to_s
				@params = {:corner => el, :format => "js"}
				post :create, @params
				response = assigns(:r)
				response.errors.should_not be_empty
				Element.count.should eq(0)

			end

			context " --- unit validation --- " do 

				it " -- corner id has to exist even with just image upload" do 

					unit = empty_unit_with_image(valid_corner)
					unit["corner_id"] = nil
					@params = {:units => [unit], :format => 'js'}	
					post :create, @params 
					response = assigns(:r)
					response.errors.should_not be_empty
					Unit.count.should eq(0)

				end

				it " -- corner id has to exist even with title and icon" do 

					unit = valid_unit
					unit["corner_id"] = nil
					@params = {:units => [unit], :format => 'js'}	
					post :create, @params 
					response = assigns(:r)
					response.errors.should_not be_empty
					Unit.count.should eq(0)

				end

				it " -- corner has to exist in database, if there is anything other than an image upload " do 
						
					c = Corner.new
					#c.versioned_create
					unit = Unit.new(:title => "anywhere", :icon => 1, :corner_id => c.id)
					attri = unit.attributes
					attri["_id"] = unit.id.to_s
					attri["corner_id"] = c.id.to_s
					@params = {:units => [attri], :format => 'js'}	
					post :create, @params 
					response = assigns(:r)
					response.errors.should_not be_empty
					Unit.count.should eq(0)					

				end


				it " -- corner has to exist in database, if there is anything other than an image upload - shud pass" do 
						
					c = Corner.new
					c.title = "hello"
					c.content = "world"
					c.save
					
					unit = Unit.new
					unit.title = "there"
					unit.icon = 1
					attri = unit.attributes
					attri["_id"] = unit.id.to_s
					attri["corner_id"] = c.id.to_s

					@params = {:units => [attri], :format => 'js'}
					post :create, @params 
					response = assigns(:r)
					response.errors.should be_empty
					Unit.count.should eq(1)					

				end				


				it " -- corner does not need to exist in database, if it is an image upload" do 

					c = Corner.new
					unit_attr = empty_unit_with_image(c)
					unit_attr["corner_id"] = c.id.to_s
					@params = {:units => [unit_attr], :format => 'js'}	
					post :create, @params 
					response = assigns(:r)
					response.errors.should be_empty
					Unit.count.should eq(1)


				end


				

			end

		end

		context " --- element not created when --- " do 

			it " --- invalid id --- " do 

				corner = valid_element
				corner["_id"] = "hello"
				@params = {:corner => corner, :format => 'js'}	
				post :create, @params 
				response = assigns(:r)
				response.errors.should_not be_empty

			end

			it " --- no id --- " do 
				
				corner = valid_element
				corner.delete("_id")
				@params = {:corner => corner, :format => 'js'}	
				post :create, @params 
				response = assigns(:r)
				response.errors.should_not be_empty			

			end


			it " --- params do not contain even one changable --- " do 

				corner = valid_element
				
				corner["_id"] = corner["_id"].to_s
				##title,content,link,link_title,image_url,image_attributes
				corner = corner.select{|k,v| k == "_id"}

				@params = {:corner => corner, :format => 'js'}	

				post :create, @params 

				response = assigns(:r)

				response.errors.should_not be_empty
				
				Element.count.should eq(0)

			end

		end

	end


	context " -- update " do 

		context " -- updates existing elements " do 

			context " -- corner" do 

				context " -- pre-existing corner with title,content" do 
					
					it " -- adds image to corner" do 
						@c = Corner.new(valid_corner)
						@c.creator_id = @user.id
						@c.content = "old content"
						@c.save
						@c.image =  File.new("app/assets/images/facebook.png")
						cattr = @c.attributes
						cattr["title"] = nil
						cattr["content"] = nil
						cattr["_id"] = @c.id.to_s 
						post :create, {:corner => cattr, :format => "js"}
						response = assigns(:r)
						response.corner.image.should_not be_nil
						response.error.should be_empty
					end


					it " -- changes corner title" do 
						@c = Corner.new(valid_corner)
						@c.creator_id = @user.id
						@c.content = "old content"
						@c.save
						@c.title = "new title"
						cattr = @c.attributes
						cattr["_id"] = @c.id.to_s	
						post :create, {:corner => cattr, :format => "js"}
						response = assigns(:r)
						response.corner.title.should eq("new title")
						response.corner.content.should eq("old content")
					end

					
				end

				context " -- pre-existing corner with image" do 


					it " -- adds corner content, title" do 
						@c = Corner.new(empty_corner_with_image)
						@c.save
						@c.title = "added a title"
						@c.image = nil
						cattr = @c.attributes
						cattr["_id"] = @c.id.to_s
							
						post :create, {:corner => cattr, :format => "js"}
						response = assigns(:r)
						response.corner.title.should eq("added a title")
					end

				end

			end

			context " -- units" do 

				context " -- pre-existing unit with icon,title" do 


					it " -- adds image" do 
						@u = Unit.new(valid_unit)
						@u.save
						@u.image =  File.new("app/assets/images/facebook.png")
						uattr = @u.attributes
						uattr["_id"] = @u.id.to_s 
						put :update, :id => @u.id, :units => [uattr], :format => "js"
						response = assigns(:r)
						response.units.each do |unit|
							unit.image.should_not be_nil
						end
					end

					it " -- changes title" do 
						@c = Corner.new
						@c.save
						@u = Unit.new(:title => "hello",:icon => 1, :corner_id => @c.id, :creator_id => @user.id)
						@u.save
						@u.title = "new title"
						uattr = @u.attributes
						uattr["_id"] = @u.id.to_s
						uattr["corner_id"] = @u.corner_id.to_s
						put :update, :id => @u.id, :units => [uattr], :format => "js"
						response = assigns(:r)
						response.errors.should be_empty
						response.units[0].title.should eq("new title")
						
					end

					it " -- chnages icon" do 
						@c = Corner.new
						@c.save
						@u = Unit.new(:title => "hello", :icon => 2, :corner_id => @c.id, :creator_id => @user.id)
						@u.save
						@u.icon = 1
						uattr = @u.attributes
						uattr["_id"] = @u.id.to_s
						uattr["corner_id"] = @u.corner_id.to_s
						
						put :update, :id => @u.id, :units => [uattr], :format => "js"
						response = assigns(:r)
						response.errors.should be_empty
						response.units[0].icon.should eq(1)
						
					end

					it "refuses to delete icon, or title alone" do 

					end

				end

				context " -- pre-exiting unit with image" do 
		
					it " -- adds title and icon" do 
						@c = Corner.new
						@c.save
						@u = Unit.new(empty_unit_with_image(@c))
						@u.corner_id = @c.id
						@u.save	

						##setup done

						@u.icon = 1
						@u.title = "title"
						uattr = @u.attributes
						uattr["_id"] = @u.id.to_s
						uattr["corner_id"] = @u.corner_id.to_s
						put :update, :id => @u.id, :units => [uattr], :format => "js"
						response = assigns(:r)
						response.errors.should be_empty
						response.units[0].icon.should eq(1)
						response.units[0].title.should eq("title")
						response.units[0].image.should_not be_nil
					end

					it " -- does not add only title " do 
						@c = Corner.new
						@c.save
						@u = Unit.new(empty_unit_with_image(@c))
						@u.corner_id = @c.id
						@u.save	

						##setup done

						@u.title = "title"
						uattr = @u.attributes
						uattr["_id"] = @u.id.to_s
						uattr["corner_id"] = @u.corner_id.to_s
						puts "the uattrs are"
						puts uattr.to_s
						put :update, :id => @u.id, :units => [uattr], :format => "js"
						response = assigns(:r)
						response.errors.should_not be_empty
					end


					it " -- does not add only icon " do 
						@c = Corner.new
						@c.save
						@u = Unit.new
						@u.image = File.new("app/assets/images/facebook.png")
						#@u.title = "test"
						#@u.icon = 1
						@u.corner_id = @c.id
						@u.save	
						
						@u.icon = 1
						uattr = @u.attributes
						uattr["_id"] = @u.id.to_s
						uattr["corner_id"] = @u.corner_id.to_s
						put :update, :id => @u.id, :units => [uattr], :format => "js"
						response = assigns(:r)
						response.errors.should_not be_empty
					end

				end

			end

		end


		context " -- updates existing and adds new units to corner" do 

			##so now we have to just test what happens when we 
			##suppose we add a new unit with only title and icon, 
			##and we also
			
			

			it " -- updates existing corner and unit, and adds a new unit " do 
				@c = Corner.new
				@c.title = "hello"
				@c.content = "there"
				@c.image = File.new("app/assets/images/facebook.png")
				@c.creator_id = @user.id
				@c.versioned_create

				@u = Unit.new
				@u.title = "unit_title"
				@u.icon = 2
				@u.image = File.new("app/assets/images/facebook.png")
				@u.corner_id = @c.id
				@u.creator_id = @user.id
				@u.versioned_create


				##########setup complete

				@c.title = "updated corner title"
				@u.title = "updated unit title"

				u2 = Unit.new
				u2.title = "appended unit"
				u2.icon = 1


				cattr = @c.attributes
				cattr["_id"] = @c.id.to_s

				u2_attr = u2.attributes
				u2_attr["_id"] = u2.id.to_s
				u2_attr["corner_id"] = @c.id.to_s
				
				uattr = @u.attributes
				uattr["_id"] = @u.id.to_s
				uattr["corner_id"] = @u.corner_id.to_s
				

				post :create, {:units => [uattr,u2_attr], :corner => cattr, :format => "js"}
				response = assigns(:r)
				response.errors.should be_empty
				response.corner.title.should eq("updated corner title")
				response.units.size.should eq(2)
				response.units.each do |u|
					if u.id.to_s == uattr["_id"]
						u.title.should eq("updated unit title")
					elsif u.id.to_s == u2_attr["_id"]
						u.title.should eq("appended unit")
					end
				end
			end
		end

	end

end
=end
require "rails_helper"

describe UnitsController do 

	login_user
=begin
	context "-- rendering a new unit " do 

		it "-- returns error if no corner id" do 

			@params = {:format => 'js'}	
			xhr :get, :new, @params 
			unit = assigns(:unit)
			unit.should_not be_nil
			unit.errors.should_not be_empty
					
		end

		it "-- returns error if invalid corner id" do 

			@params = {:corner_id => "dog", :format => 'js'}	
			xhr :get, :new, @params 
			unit = assigns(:unit)
			unit.should_not be_nil
			unit.errors.should_not be_empty

		end


		it "-- returns unit if legal corner id" do 


			
			@params = {:corner_id => BSON::ObjectId.new.to_s, :format => 'js'}	
			xhr :get, :new, @params 
			unit = assigns(:unit)
			unit.should_not be_nil
			unit.errors.should be_empty

		end

	end
=end
end
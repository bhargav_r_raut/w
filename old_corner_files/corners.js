var corners_on_load = function(){
	if(should_js_run("corner") === false){
		//console.log("returned from corners");
		return;
	}
	
	$('.icon-tooltip').tooltipster({
	    delay : 0,
	    theme : 'icon-tooltip-template',
	    hideOnClick : false
	});

	$('.add-section-tooltip').tooltipster({
	    delay : 0,
	    theme : 'add-section-tooltip-template',
	    hideOnClick : false,
	    position: 'top'
	});

	$('.sun-tooltip').tooltipster({
	    delay : 0,
	    theme : 'sun-tooltip-template',
	    hideOnClick : false,
	    position: 'left'
	});

	$('.rain-tooltip').tooltipster({
	    delay : 0,
	    theme : 'rain-tooltip-template',
	    hideOnClick : false,
	    position: 'left'
	});

	$('.storm-tooltip').tooltipster({
	    delay : 0,
	    theme : 'storm-tooltip-template',
	    hideOnClick : false,
	    position: 'left'
	});

	
	 $(document).ready(function(){
	    $('.scrollspy').scrollSpy();
	 });

	 $(document).ready(function(){
	    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered

	    $('.editable_images').leanModal({

	    	abort_if_not_class: "mdi-image-photo-camera",
	    	abortion_applicable_to: "intro_icon",
	    	ready: function(options) 
		    { 	
		    	//assign the value of whatever is the opener id to the image url submit and image upload data-image-for fields.
		    	$("#image_url_submit").attr("data-opener-id",options.opener_id);
		    	$("#upload_image_from_pc").attr("data-opener-id",options.opener_id);
		    },
		    complete: function(options){
		    	//remove those values.
		    	$("#image_url_submit").attr("data-opener-id","");
		    	$("#upload_image_from_pc").attr("data-opener-id","");
		    	//clear the input values from the modal.
		    	$("#image_url").val("");
		    	$("#image_url_submit").hide();
		    }
	    });

	    $('.editable_links').leanModal({

	    	dismissible: true, // Modal can be dismissed by clicking outside of the modal
		    opacity: .5, // Opacity of modal background
		    in_duration: 300, // Transition in duration
		    out_duration: 200, // Transition out duration
		    close_on_clicking: "#save_link",
		    close_on_clicking_function: function(options)
		    {

		    	
		    	//validate
		    	if(validate_link($("#link-text").val())){
		    		$("#link-error-message").hide();
					$("#link-text").removeClass("invalid");
					$("#link-text").addClass("valid");
		    		$("#close_modal").trigger("click");
		    		
		    	} 
		    	else{

		    		$("#link-error-message").show();
					$("#link-text").addClass("invalid");
					$("#link-text").removeClass("valid");
		    		
		    	}
		    },
		    ready: function(options) 
		    { 	
		    	
			    $("#link-text")
			    .val($("#" + options.opener_id).attr("data-link"));
			    $("#link-title")
			    .val($("#" + options.opener_id).attr("data-link-title"));
		    }, 
		    complete: function(options) {
		    	if(validate_link($("#link-text").val())){
		    		//alert("is valid");
		    		activate_link_icon("#" + options.opener_id);
		    	} 
		    	else{
		    		//alert("is invalid");
		    		deactivate_link_icon("#" + options.opener_id);	
		    	}
		    	
		    	//empty the box, because next time it could be used for another link.
		    	

		    	//if the guy is closing , even if its invalid, we remove all the
		    	//invalid markers, when he opens it the next time, he can get a fresh empty thing.
		    	//if it was valid, this will not be necessary anyways.
		    	$("#link-error-message").hide();
				$("#link-text").removeClass("invalid");
				if($("#link-text").val().trim() === ""){
					$("#link-text").removeClass("valid");
				}
		     } 

	    });
	 });
	


	
}


$(document).ready(corners_on_load);
$(document).on('page:load', corners_on_load)
;


var activate_link_icon = function(id){
	$(id).attr("data-link",$("#link-text").val());
	$(id).attr("data-link-title",$("#link-title").val());
	//remove the teal-text and grey-text and text-lighten-2 classes
	$(id).removeClass("grey-text");
	$(id).removeClass("teal-text");
	$(id).removeClass("text-lighten-2");
	//add the amber class.
	$(id).addClass("amber-text");

	//make the defaultcolor and hovercolor the same
	$(id).attr("data-defaultcolor","amber-text");
	$(id).attr("data-hovercolor","amber-text");

	//hide the dropdown arrow, so that this always shows.
	$(id).parent().parent().prev().hide();
};

var deactivate_link_icon = function(id){
	$(id).attr("data-link","");
	$(id).attr("data-link-title","");
	//remove the teal-text and grey-text and text-lighten-2 classes
	$(id).addClass("grey-text");
	$(id).addClass("text-lighten-2");
	//add the amber class.
	$(id).removeClass("amber-text");

	//make the defaultcolor and hovercolor the same
	$(id).attr("data-defaultcolor","grey-text");
	$(id).attr("data-hovercolor","teal-text");

	//hide the dropdown arrow, so that this always shows.
	$(id).parent().parent().prev().show();
}

/**
for locked links, show them on click.
**/
$(document).on('click','.locked_links',function(event){
	/**
	$(this).parent().parent().next().toggle();
	$(this).parent().parent().next().children().first().children().first().html($(this).attr("data-link"));
	**/
	open_new_tab($(this).attr("href"));

});


//when clicked on the contenteditable element.
//if the class is that of the final class
//then dont do anything.

//else 
//make its html nil, and the html of the next element i.e the eg nil.


//when clicking anywhere else in the document.
//if the target element has nothing in it,
//then put back the default class and the default content of the content editable.
//else apply the final class of the contenteditable to the element.

$(document).on('click','body',function(event){

	
	var clicked_element_unit_id;

	//console.log("THE EVEN TARGET ID IS:" + event.target.id);

	//if we clicked on the weather icons, then we know we are clicking a section
	if($("#" + event.target.id).parent().parent().hasClass("weather_holder")){
		//then get the data-unit.
		clicked_element_unit_id = 
		$("#" + event.target.id).parent().parent().parent().attr("data-unit");
	}

	for(editable_element in gon.content_editable_defaults){
		var editable_element_current_html = $(editable_element).html();

		//provided that the element we are iterating is not the clicked element.
		if(event.target.id!= gon.content_editable_defaults[editable_element]["name"]){
			if(editable_element_current_html.trim().length === 0){
				//reset it back to the placeholder.
				$(editable_element).html(gon.content_editable_defaults[editable_element]["default_content"]);
				$(editable_element).attr("class",gon.content_editable_defaults[editable_element]["default_class"]);
				if(editable_element === "#about"){
					$("#about_example").html(gon.content_editable_defaults[editable_element]["example_element_default_content"]);
				}
				
				

			}
			else{
				
			
				if(editable_element === "#intro" && editable_element_current_html != gon.content_editable_defaults[editable_element]["default_content"]){
						
						

					
						
						$("#intro_icon").attr("class","mdi-image-photo-camera editable_images");			
						$("#intro_icon").css("cursor","pointer");
						$("#intro_icon").parent().attr("title","Upload An Image");
						$("#intro_icon").parent().tooltipster({
							delay : 0,
						    theme : 'add-section-tooltip-template',
						    hideOnClick : false,
						    position: 'top'
						});
						
						
					
				}
				else if(editable_element === "#about" && editable_element_current_html != gon.content_editable_defaults[editable_element]["default_content"]){
					//replace the example with the user name stored on the page in a 
					//data variable or accessible via gon?
					//make it contenteditable.
					
					$(editable_element).next().html("By " + $(editable_element).next().attr("data-username") + " on " + moment().format('MMMM Do YYYY'));
					//$(editable_element).next().css("cursor","pointer");

				}

			}

		}
		else{
			//do the on click event.

			clicked_element_unit_id = $(editable_element).parent().parent().attr("data-unit");

			$(editable_element).attr("spellcheck",false);
				
				if($(editable_element).attr("class") === gon.content_editable_defaults[editable_element]["set_class"]){

					

				}
				else{
					
					$(editable_element).removeClass("virgin");
					$(editable_element).addClass("edited");
					$(editable_element).html("");
					$(editable_element).attr("class",gon.content_editable_defaults[editable_element]["set_class"]);
					
			}

		}

	}

	//if the weather icon holder is open and an element is selected, then collapse the box, to show only the selected element.
	$(".weather_holder").each(function(){
			
		    if (event.target == $(this)[0] || $.contains($(this)[0], event.target)) {
		       
		    } else {
		       
				if($(this).find(":visible").length === 8 && $(this).find(".set").length === 1){
					$(this).find(".set").each(function(){
						$(this).trigger("click");
					});

					

				}
		    }

			
	});

	
	
	//validate only when clicking either on a section or a weather icon.
	//validation does not happen on body clicks.
	console.log("validating all elements with:" + clicked_element_unit_id);
	if(clicked_element_unit_id!=null){
		validate_all_units(clicked_element_unit_id);
	}
		

});



$(document).on({
    mouseenter: function () {

       	
    },
    mouseleave: function () {
        
      	
        
    },
    click: function(e){
		//make the icon colored.
		$(this).removeClass("dull");

		//add the set class on it for other handlers.
		$(this).addClass("set");
		
		//make all the children of the parents siblings dull.
		$(this).parent().siblings().not(".icon_warning").children().addClass("dull");

		//unset all the above..
		$(this).parent().siblings().not(".icon_warning").children().removeClass("set");
		//collapse all the above.
		$(this).parent().siblings().not(".icon_warning").toggle("fast");



		//hide the tooltip - doesnt work as well as required, but better than not doing it at all.
		$(this).tooltipster("hide");
		$(this).parent().parent().attr("data-icon-set",true);

		//if the icon warning is seen, then get rid of it.
		$(this).parent().siblings().each(function()
		{
			if($(this).hasClass("icon_warning"))
			{
				$(this).hide();
			}
		});


    }
}, ".weather");


$(document).on({
    mouseenter: function () {

    	$(this).removeClass($(this).attr("data-defaultcolor"));
    	$(this).addClass($(this).attr("data-hovercolor"));
       	
    },
    mouseleave: function () {
        
        if($(this).attr("data-defaultcolor") != $(this).attr("data-hovercolor")){
        	$(this).addClass($(this).attr("data-defaultcolor"));
      		$(this).removeClass($(this).attr("data-hovercolor"));
      	}
        
    },
    click: function(e){
		

    }
}, ".options");







//load the views chart when the stats are clicked.
$(document).on('click','#stats_toggle',function(){

	$('#stats').slideToggle("fast",function(){
		//if the stats is visible,then load the chart.
		//if not dont do anything.
		if($("#stats").css("display") === "block"){
			console.log("got the stats as displayed");
			var ctx = $("#views_chart").get(0).getContext("2d");
		/****
		dummy data.
		***/
			var data = {
		    labels: ["4 am", "5 am", "6 am", "7 am", "12pm", "3 pm", "7 pm","9pm","10pm","12pm"],
			    datasets: [
			        {
			            label: "My First dataset",
			            fillColor: "rgba(220,220,220,0.2)",
			            strokeColor: "rgba(220,220,220,1)",
			            pointColor: "rgba(220,220,220,1)",
			            pointStrokeColor: "#fff",
			            pointHighlightFill: "#fff",
			            pointHighlightStroke: "rgba(220,220,220,1)",
			            data: [65, 59, 80, 81, 56, 55, 40,100,200,120],
			            tooltip_objects:[]
			        }
			    ]
			};
			/***
			end dummy data
			***/

			var myLineChart = new Chart(ctx).Line(data, {});
				}
			});

});




$(document).on('click','#add-section',function(event){
	var replacement = String($("#units_holder").children().length);
	$("#units_holder").append(section_unit.replace(/wordjelly/g,replacement));
	//update the gon variable

	gon.content_editable_defaults["#section_title_" + replacement] = {
		"name" : "section_title_" + replacement,
		"set_class" : "grey-text text-darken-2 fw-400 fs-18 edited wsection",
		"default_class" : "fs-18 grey-text text-lighten-1 fw-300 virgin wsection",
		"default_content": "Add Section Title",
		"example_element_default_content": ""
	}


	$("#sun_" + replacement).tooltipster({
		delay : 0,
	    theme : 'sun-tooltip-template',
	    hideOnClick : false,
	    position: 'left'
	});

	$("#rain_" + replacement).tooltipster({
		delay : 0,
	    theme : 'rain-tooltip-template',
	    hideOnClick : false,
	    position: 'left'
	});
	$("#lightning_" + replacement).tooltipster({
		delay : 0,
	    theme : 'storm-tooltip-template',
	    hideOnClick : false,
	    position: 'left'
	});

	//add the invalid trigger to the invites section,.
	$("#invalid_monkey_patching").append("<a href=\"#scrollspy_unit_\""+ replacement +" id=\"corner_section_"+ replacement +"_invalid\"></a>");	

});

$(document).on('click','#publish',function(event){

	$("#corner_publish").val(1);
	submit_corner();

});


$(document).on('click','.icon_warning',function(event){
	$(this).tooltipster("show");
});





var validate_link = function(link){
	return /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i.test( link);
}

/***
*returns null if the about is valid.
*otherwise returns the scrollspy_id of the about element.
***/
var validate_corner_about = function(){
	//if the val is the default val or is empty, it needs to be filled.
	var about_content = $("#about").html();
	
	if(about_content === gon.content_editable_defaults["#about"]["default_content"] || about_content.trim().length() === 0){
		//invalid.
		$("#corner_title_invalid").show();
		return "#scrollspy_about";
	}
	else{
		$("#corner_title_invalid").hide();
		return null;
	}
}



/****
* sets the warnings on all the invalid units.
* returns null if there are no invalid units, otherwise returns the scrollspy id of the first invalid unit
*
*****/
var validate_all_units = function(clicked_unit_index){

	//the scroll point for the first invalid unit.
	var first_invalid_unit_scroll_id = null;
	//console.log("------------------NEW ALL VALIDATION STARTS------------------------\n\n");
	//console.log("entered validate all inputs");

	$(".corner_unit").each(function(index){


		var corner_unit_id = $(this).attr("data-unit");
		
		//console.log("validating corner unit id:" + corner_unit_id);


		if(corner_unit_id!=null){


			var unit_title = $(this).find("#section_title_" + corner_unit_id);

			var unit_is_icon_set = $(this).children(".weather_holder").first().attr("data-icon-set") === "true"? true : false;
			var unit_title_virgin = unit_title.hasClass("virgin");


			//console.log("unit title text is:" + unit_title.text());
			//console.log("is unit title virgin:" + unit_title_virgin);
			//console.log("unit icon is set:" + unit_is_icon_set);


			if(clicked_unit_index!=null && corner_unit_id == clicked_unit_index){
				//dont validate it because we are currently interacting with it.
				//console.log("we are validating whatever was clicked on, so we dont do anything." + corner_unit_id);
			}
			else{
				//validate it because this is either not what we are interacting with or 
				//we are validating pre publish.
				//console.log("we are validating something else");
				if(!unit_is_icon_set && unit_title_virgin){
					//we dont do any validations on it.
					//console.log("!!!detected that the element is totally untouched"+ corner_unit_id +"!!!");
				}
				else if(unit_is_icon_set && !unit_title_virgin){
					//it is valid, so dont do anything.
					//console.log("!!!detected that the element is valid"+ corner_unit_id +"!!");
				}
				else{
					
					if(!unit_is_icon_set){
						//if the icon is not set -> show the icon warning.
						//console.log("!!detected that the icon is not set"+ corner_unit_id +"!!");
						$("#icon_invalid_" + corner_unit_id).show();

					}
					if(unit_title_virgin){
						//if the title is virgin -> show the title warning.
						//console.log("!!detected that the title is invalid."+ corner_unit_id +"!!");
						$("#title_invalid_" + corner_unit_id).show();

					}

					if(first_invalid_unit_scroll_id === null){
							first_invalid_unit_scroll_id = "scrollspy_unit_" + corner_unit_id;
					}
					
				}
			}
			

		}
		else{

			//someone is playing with the dom.
		
		}
		

	});
	
	
	//console.log("-----------ALL validation ends------------------" + new Date())
	return first_invalid_unit_scroll_id;

}

var section_unit = "<div class=\"row m-20-bottom\" id=\"corner_unit_wordjelly_holder\" data-unit=\"wordjelly\"><div class=\"section scrollspy\" id=\"scrollspy_unit_wordjelly\"></div><div class=\"col l1 m1 s1 p-10-top p-10-bottom center blue-grey-text text-darken-2 fw-300 fs-25 weather_holder\" style=\"border:1px solid gainsboro; width:11.33%;\" id=\"section_weather_wordjelly\"><input type=\"hidden\" name=\"corner[units_attributes][1][icon]\" id=\"corner_units_attributes_wordjelly_icon\"><div class=\"fs-30 blue-grey-text text-darken-2\"><i class=\"wi wi-day-sunny dull weather sun-tooltip \" id=\"sun_wordjelly\" title=\"Bright\"></i></div><div style=\"height:20px;\"></div><div class=\"fs-30 blue-grey-text text-darken-2\"><i title=\"Rainy\" class=\"wi wi-day-rain dull weather rain-tooltip \" id=\"rain_wordjelly\"></i></div><div style=\"height:20px;\"></div><div class=\"fs-30 blue-grey-text text-darken-2\"><i class=\"wi wi-lightning dull weather storm-tooltip\" title=\"Stormy\" id=\"lightning_wordjelly\"></i></div></div><div class=\"col l9 m9 s9 p-20-top p-18-bottom center m-10-left\" style=\"border:1px solid gainsboro; position:relative\" id=\"section_title_holder_wordjelly\"><div class=\"fs-18 grey-text text-lighten-1 fw-300 virgin wsection\" id=\"section_title_wordjelly\" contenteditable=\"true\">Add A Short Section Title</div><div class=\"row m-0-bottom options_holder m-5-top\" style=\"display:none;\"><div class=\"col l6 offset-l3 m6 offset-m3 s6 offset-s3 center\"><i class=\"mdi-navigation-cancel grey-text text-lighten-2 fs-15 options\" data-hovercolor=\"red-text\" data-defaultcolor=\"grey-text\"></i><i class=\"mdi-content-link grey-text text-lighten-2 fs-15 options\" data-hovercolor=\"teal-text\" data-defaultcolor=\"grey-text\"></i><i class=\"mdi-image-photo-camera grey-text text-lighten-2 fs-15 options\" data-hovercolor=\"blue-grey-text\" data-defaultcolor=\"grey-text\"></i></div></div><input type=\"hidden\" name=\"corner[units_attributes][1][title]\" id=\"corner_units_attributes_wordjelly_title\"></div><div id=\"section_body_and_image_holder_wordjelly\"><input type=\"hidden\" value=\"\" name=\"corner[units_attributes][1][image_url]\" id=\"corner_units_attributes_wordjelly_image_url\"></div></div>";


$(document).on('click','#section_link_dropdown',function(event){
	$(this).toggleClass("mdi-hardware-keyboard-arrow-down");
	$(this).toggleClass("mdi-hardware-keyboard-arrow-up");
	$(this).next().toggle();
});


/***
handling showing of the submit the image via url for the add al link modal.
1.if the text input is clicked, the icon will be show.
2.on clicking anywhere in the modal, if the text input is empty, the icon will be hidden.

***/
$(document).on('click',"#image_url",function(event){


	$("#image_url_submit").show();


});


$(document).on('click',"#upload_an_image_modal",function(event){

	if(event.target.id!="image_url"){	
		if($("#image_url").val().trim().length === 0){
			$("#image_url_submit").hide();
		}
	}

});

//handling the submission of a url instead of an image.
$(document).on('click','#image_url_submit',function(event){
	var image_url = $("#image_url").val();
	var is_url_valid = validate_link(image_url);
	if(is_url_valid){
		//then 
		$("#image-url-error-message").hide();


		if($(this).attr('data-opener-id') === "intro_icon"){

			$("#corner_image_url").val(image_url);
			$("#corner_image_url").trigger("change");
			
		}
		else{
			//its one of the sections.
			//in that case get its relevant uimage_url field and set this value to that.
			
			var unit_id = get_unit_id_from_string_id($(this).attr('data-opener-id'));
			$("#corner_units_attributes_" + unit_id + "_uimage_url").val(image_url);
			//$("#corner_units_attributes_" + unit_id + "_uimage_url").trigger("change");
			$("#corner_units_attributes_" + unit_id + "_uimage_url").trigger("change");
		}
		
	}
	else{
		//show the error message.
		$("#image-url-error-message").show();
	}
});


//handling image file chosen from pc.
$(document).on("click","#upload_image_from_pc",function(event){


	var opener_id = $(this).attr("data-opener-id");
	//trigger a click on the file_field that belongs to this opener id.
	if(opener_id === "intro_icon"){
		$("#corner_image").trigger("click");
	}		
	else{
		var opener_unit = get_unit_id_from_string_id(opener_id);
		$("#corner_units_attributes_" + opener_unit + "_uimage").trigger("click");
	}


});

//handles the actual submit of the form, with dismissing of modals and showing the
//preloader,and handles the response.

var submit_ajax = function(modal_id){

	//if modal id is not null, show the modal spinner.
	if(modal_id!=null){
		$("#" + modal_id + "_preloader").show();
	}
	$("#new_corner").trigger("submit.rails");
	//all after submit functionality will be handled by 
}

//the only function that should be called to post the form.
var submit_corner = function(modal_id){	

	//get value of publish.
	if($("#corner_publish").val() === 1){
		//validate all units and title.
		about_valid = validate_corner_about();
		units_valid = validate_all_units();
		if(about_valid===null && units_valid===null){
			//go ahead and submit.
			//$("#new_corner").trigger("rails.submit");
			submit_ajax(modal_id);
		}
		else{
			//click on the hidden buttons in the invite section which trigger the 
			//scrollspy actions.
			if(about_valid){
				$("#corner_about_invalid").trigger("click");	
			}
			else{
				var unit_id = get_unit_id_from_string_id(units_valid);
				$("#corner_section_"+ unit_id +"_invalid").trigger("click");
			}
		}
	}
	else{
		console.log(new Date() + "= called submit ajax , publish is 0 =");
		//go ahead and submit.
		//$("#new_corner").trigger("rails.submit");
		submit_ajax(modal_id);
	}


}


//on choosing an image or on inputing a valid url, submit corner should get called.
$(document).on('change','#corner_image_url,.uimage_url,#corner_image,.uimage',function(event){

	submit_corner("upload_an_image_modal");

});




var get_unit_id_from_string_id = function(string_id){
	return string_id.replace(/\D/g, '');
}



